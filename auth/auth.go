//This package manages the authentication logic (using Google Oauth)
//Package Init function *http.Client
package auth;

import (
    "encoding/json"
    "fmt"
  . "gitlab.com/mrvik/logger"
  . "gitlab.com/mrvik/gogdrive/types"
    "golang.org/x/net/context"
    "golang.org/x/oauth2"
    "golang.org/x/oauth2/google"
    "io/ioutil"
    "net/http"
    "os"
)

var (
    cnf *Config;
    log Logger;
)

//Read token from file or stdin and return *http.Client
func Init(ctx context.Context, lg Logger, cnfp *Config) *http.Client{
    cnf=cnfp;
    log=lg;
    defer recoverAuthentication();
    oauthClient:=generateClient(ctx);
    return oauthClient;
}

func generateClient(ctx context.Context) *http.Client{
    content,err:=ioutil.ReadFile(*cnf.Files.Credentials);
    if os.IsNotExist(err) {
        panic(fmt.Sprintf("Place the credentials file on %s\n", *cnf.Files.Credentials));
    }else{
        checkErr(err);
    }
    config,err:=google.ConfigFromJSON(content, cnf.Scopes);
    checkErr(err);
    return getClient(ctx, config);
}

func getClient(ctx context.Context, config *oauth2.Config) *http.Client {
    token,err:=readToken();
    if err != nil && os.IsNotExist(err) {
        token,err=tokenFromWeb(ctx, config);
        checkErr(err);
        err=saveToken(token);
        checkErr(err);
    } else if err != nil {
        panic(err);
    }
    return config.Client(ctx, token);
}

func tokenFromWeb(ctx context.Context, config *oauth2.Config) (*oauth2.Token,error) {
    url:=config.AuthCodeURL("state-token", oauth2.AccessTypeOffline);
    fmt.Printf("Go to this URL, login and paste the token back here\n%s\n", url);
    var code string;
    if _,err:=fmt.Scan(&code); err != nil {
        return nil,err;
    }
    token,err:=config.Exchange(ctx, code);
    return token,err;
}

func saveToken(token *oauth2.Token) error {
    path:=*cnf.Files.Token;
    log.Infof("Saving token to %s\n", path);
    fd,err:=os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600);
    checkErr(err);
    defer fd.Close();
    err=json.NewEncoder(fd).Encode(token);
    checkErr(err);
    return nil;
}

func readToken() (*oauth2.Token, error){
    fd, err:=os.Open(*cnf.Files.Token);
    if err != nil {
        return nil,err;
    }
    defer fd.Close();
    token:=&oauth2.Token{};
    err=json.NewDecoder(fd).Decode(token);
    checkErr(err);
    return token,nil;
}

func checkErr(err error){
    if err != nil {
        panic(err);
    }
}

func recoverAuthentication(){
    if e:=recover(); e != nil {
        log.Errorf("Error while authenticating: %s\n", e);
        os.Exit(1);
    }
}
