//Custom errors
package types;

import(
    "fmt"
)

type ArgumentsError struct{
    string
}

func NewArgumentsError(format string, arguments ...interface{}) ArgumentsError{
    return ArgumentsError{"Arguments Error: "+fmt.Sprintf(format, arguments...)};
}

func (ae ArgumentsError) Error() string{
    return ae.string;
}

type UnimplementedType struct{
    string
}

func NewUnimplementedType(typ string) UnimplementedType{
    return UnimplementedType{fmt.Sprintf("Type %q not implemented", typ)};
}

func (ut UnimplementedType) Error() string{
    return ut.string
}

type IsFileError struct{
    string
}

func NewIsFileError(path string) IsFileError{
    return IsFileError{fmt.Sprintf("Path %q is an existent file", path)};
}

func (sf IsFileError) Error() string{
    return sf.string;
}

type ShadowFileError struct{
    string;
}

func NewShadowFileError(id string) ShadowFileError{
    return ShadowFileError{
        fmt.Sprintf("Got a shadow file with ID %q", id),
    };
}

func (sf ShadowFileError) Error() string{
    return sf.string;
}

type TooManyErrors struct{
    string;
}

func NewTooManyErrors(errors, max int) TooManyErrors{
    return TooManyErrors{fmt.Sprintf("Too many errors: %d. Max: %d\n", errors, max)};
}

func (er TooManyErrors) Error() string{
    return er.string;
}

type EntryExistsError struct{
    string;
}

func NewEntryExistsError(entry string) EntryExistsError{
    return EntryExistsError{fmt.Sprintf("Entry %s already exists on map", entry)};
}

func (er EntryExistsError) Error() string{
    return er.string;
}

type EntryNotFoundError struct{
    string;
}

func NewEntryNotFoundError(entry string) EntryNotFoundError{
    return EntryNotFoundError{fmt.Sprintf("Entry %s doesn't exist on mao", entry)};
}

func (er EntryNotFoundError) Error() string{
    return er.string;
}

//Error generating or locating a path
type PathError struct{
    Msg, Path, ID string;
}

func NewPathError(format, fileNameOrPath, fileId string) PathError{
    return PathError{
        Msg: fmt.Sprintf(format, fileNameOrPath, fileId),
        Path: fileNameOrPath,
        ID: fileId,
    }
}

func (pe PathError) Error() string{
    return pe.Msg;
}

//File already exists on conversion tree
type FileExistsError struct{
    Msg, Path, ID string;
}

func NewFileExistsError(fileNameOrPath, fileId string) FileExistsError{
    format:="File %q(%s) already exists";
    return FileExistsError{
        Msg: fmt.Sprintf(format, fileNameOrPath, fileId),
        Path: fileNameOrPath,
        ID: fileId,
    }
}

func (pe FileExistsError) Error() string{
    return pe.Msg;
}

//File doesn't exist on conversion tree
type FileNotExistError struct{
    Msg, Path, ID string;
}

func NewFileNotExistError(fileNameOrPath, fileId string) FileNotExistError{
    format:="File %q(%s) doesn't exist";
    return FileNotExistError{
        Msg: fmt.Sprintf(format, fileNameOrPath, fileId),
        Path: fileNameOrPath,
        ID: fileId,
    }
}

func (pe FileNotExistError) Error() string{
    return pe.Msg;
}
