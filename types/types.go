//This package provides types for the other packages here.
//Functions here have no complicated logic so tests aren't needed.
package types;

import (
    "context"
  drv "google.golang.org/api/drive/v3"
    "os"
    "time"
)

const SCOPES = "https://www.googleapis.com/auth/drive"
const ( //Option constants
    //Is this request allowed to fail? If a request fails, the worker desestimates the complete slice, but some requests may fail.
    O_REQ_MAY_FAIL=1<<0;
    //Process a request anyway after some req before did fail
    O_REQ_DO_ANYWAY=1<<1;
)

// Config structures
type Config struct{
    Base *string;
    Files ConfigFiles;
    Local LocalConfig;
    Drive DriveConfig;
    Events EventsConfig;
    Socket SocketConfig;
    AuthOnly, DryRun *bool;
    ID *string;
    Scopes string;
}

type ConfigFiles struct{
    UserConfig, Credentials, Token, Lockfile, Ignore, LocalDir *string;
}

type LocalConfig struct{
    Modes struct{
        File, Dir os.FileMode;
    }
}

type DriveConfig struct{
    Workers uint;
    AllowSharedFiles, DeleteForever, AllowDriveFiles bool;
    RemoveOrphans *bool;
    UpdateAtime bool;
}

type EventsConfig struct{
    Workers uint;
    MaxRetries uint;
}

type SocketConfig struct{
    Path string;
}

func NewConfig(init ...*Config) Config{
    rt:=Config{
        Files: ConfigFiles{},
    };
    if len(init) >= 1 {
        rt=*init[0];
    }
    rt.Scopes=SCOPES; //It must not be changed
    return rt;
}

// Events fired from Localfs or Drive
//
//Event types:
//- CREATE -> Triggered when a file is added to the hierarchy. This triggers one side sending it to the other one and adding it to the conversion tree when it happens on Drive.
//- REMOVE -> Triggered when a file or directory is removed. This triggers the remotion of it's counterpart on the other side an on conversion tree when it happens on Drive.
//- WRITE -> Triggered when a file is updated. This triggers a update on the other side.
//- RENAME -> When a file is renamed, it's stored waiting for confirmation from a CREATE event and done after the second RENAME event on the old file
//- CHMOD -> When the attributes of a file changes, this event is fired. May be a date or permissions change.
type Event struct {
    //Type holds the type of the event as reported by fsnotify.Event.String() and splitted. All uppercase.
    //Type fsnotify bundles multiple events for the same file separated by pipes. We split them on different events.
    Type string;
    //Name holds the filename where the event comes from
    //Path holds the path relative to the watch dir
    //FullPath holds the full path on the filesystem to the specified object. Not present on remote events
    //FileID holds the FileID reported by te file's xattrs. It can be an empty string for new files or on filesystems w/o xattrs(5)
    Name, Path, FullPath, FileID string;
    //OldPath holds the old file path on move requests
    OldPath string;
    //File holds the current value for the drive file. On REMOVE events it holds a cached value
    File *drv.File;
    //Origin holds a tiny int value for where the event was originated. 0-> Local, 1-> Remote
    Origin byte;
    //Propagate holds a boolean depending if the event should be listened out of the localfs package. Usually it's always true
    Propagate bool;
    //Time holds the time where the event was fired
    Time time.Time;
    //The fields here are only filled when the type is an error
    //Error holds an error ocurred on Location
    Error error;
    //Message holds the error string
    Message string;
    //Location contains the package where the error was originated
    Location string;
}

//Localfs requests
type Request struct{
    //Type holds the type of request
    Type string;
    //Arguments holds a slice of string parameters for the function
    Arguments []string;
    //Path holds the path to the file affected
    Path string;
    //File holds a pointer to os.File or nil. This can be used to start a reader or a writer
    File *os.File;
    //Destination of the Request. 0 -> Local IO. 1 -> Drive IO. 2 -> Worker thread (blacklist helpers)
    //Destination is only needed when delegating Requests on a workerThread which has to route them
    Destination byte;
    //Options admit any option from the O_REQ_*
    Options byte;
    //Context allows us to track the Request progress and cancel it if needed
    Context context.Context;
    //Cancel function is called when the event is fullfilled
    Cancel context.CancelFunc;
    //Status is a channel who returns an error or nil. Functions send status via this channel when they're ready. After it's been read, the channel is closed
    Status chan error;
    //ReturnValue is populated with a return value. It's populated before Status and should be read after waiting for status. You know the type
    ReturnValue interface{};
}

//Initialize a IO Request with type, path and additional arguments
func NewRequest(typeOfRequest, path string, arguments ...string) *Request{
    return &Request{
        Type: typeOfRequest,
        Path: path,
        Arguments: arguments,
        Status: make(chan error, 1), //Allow a worker to set status w/o waiting for another thread to read it. Some request status aren't read
    }
}

//Initialize a IO Request with context, path and additional arguments
func NewRequestWithContext(baseContext context.Context, typeOfRequest, path string, arguments ...string) *Request{
    r:=NewRequest(typeOfRequest, path, arguments...);
    r.Context, r.Cancel=context.WithCancel(baseContext);
    return r;
}

//The File type wraps a Drive File with its path.
//About nameless fields, take a look at https://golang.org/ref/spec#Struct_types
type File struct{
    *drv.File;
    //Local file path
    Path string;
}

func NewFile(drvFile *drv.File, path string) *File{
    return &File{
        File: drvFile,
        Path: path,
    }
}
