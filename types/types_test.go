package types;

import(
    "context"
    "fmt"
    "math/rand"
    "runtime"
    "strings"
    "testing"
    "testing/quick"
    "time"
)

const (
    CONDITION_DURATION=time.Millisecond*100;
)

//Configurations
var(
    resolveMapConfig=&quick.Config{
        MaxCount: 10,
    }
    eventWaitConfig=&quick.Config{
        MaxCount: 10,
    }
    mutexedListConfig=&quick.Config{
        MaxCount: 20,
    }
)

func TestResolveMap(t *testing.T){
    masterContext, masterCancel:=context.WithCancel(context.Background());
    defer masterCancel();
    mp:=NewResolveMap(masterContext);
    //Test concurrent access
    f:=func(numIter uint8) error{
        t.Logf("Iterating %d times\n", numIter);
        cancels:=make([]context.CancelFunc, numIter);
        var i uint8;
        errors:=make(chan error, numIter);
        for i=0; i<numIter; i++ {
            key:=fmt.Sprintf("entry number %d", i);
            ctx, cancel:=context.WithTimeout(masterContext, CONDITION_DURATION);
            cancels[i]=cancel;
            go func(){
                mp.Add(key, ctx);
                if _,exists:=mp.Check(key); !exists{
                    errors<-fmt.Errorf("a key %q was added just now and doesn't exist", key);
                }
            }();
        }
        var (
            errorBuilder strings.Builder;
            gotErrors bool;
        )
        errorBuilder.WriteString("errors from list handle:\n");
        loop:
        for{
            select{
            case e:=<-errors:
                errorBuilder.WriteString("\t"+e.Error()+"\n");
                gotErrors=true;
            default:
                break loop;
            }
        }
        if gotErrors {
            return fmt.Errorf(errorBuilder.String());
        }
        cancelAll(cancels...);
        t.Log("Sleeping to leave time for contexts to timeout");
        time.Sleep(CONDITION_DURATION);
        mp.removeStaleObjects(); //Manually run the housekeeping routine
        mp.mutex.Lock();
        defer mp.mutex.Unlock();
        if ln:=len(mp.mp); ln > 0{
            return fmt.Errorf("all contexts are done but map still has %d values", ln);
        }
        return nil;
    }
    if err:=quick.CheckEqual(f, alwaysNil, resolveMapConfig); err != nil{
        t.Error(err);
    }
}

func BenchmarkResolveMapWithBigMap(b *testing.B){
    ctx, cancel:=context.WithCancel(context.Background());
    defer cancel();
    mp:=NewResolveMap(ctx);
    for i:=0; i<400; i++{
        mp.Add("string "+string(i), ctx);
    }
    b.ResetTimer();
    for i:=0; i<b.N; i++{
        mp.Check("string "+string(i));
    }
}

func BenchmarkResolveMapWithSmallMap(b *testing.B){
    ctx, cancel:=context.WithCancel(context.Background());
    defer cancel();
    mp:=NewResolveMap(ctx);
    mp.Add("string 1", ctx);
    b.ResetTimer();
    for i:=0; i<b.N; i++{
        mp.Check("string "+string(i));
    }
}

func BenchmarkResolveMapAdd(b *testing.B){
    ctx, masterCancel:=context.WithTimeout(context.Background(), CONDITION_DURATION);
    defer masterCancel();
    mp:=NewResolveMap(ctx);
    defer masterCancel();
    b.ResetTimer();
    for i:=0; i<b.N; i++ {
        mp.Add("someFile"+string(i), ctx);
    }
}

func cancelAll(cancelFuncs ...context.CancelFunc){
    for _,fn:=range(cancelFuncs){
        fn();
    }
}

func alwaysNil(n uint8) error{
    return nil;
}

func alwaysNilString(_ string) error{
    return nil;
}

func alwaysInt(num int) func(string) int{
    return func(_ string) int{
        return num;
    }
}

func TestEventWaitPool(t *testing.T){
    testFn:=func(path string) int{
        events:=make(chan *Event, 10);
        pool:=NewEventWaitPool(events, 200);
        timeout:=time.After(time.Millisecond*2000); //Skip last 20ms
        exit:=false;
        foloop:
        for i:=0; i<20000000 || exit; i++ {
            select{
            case <-timeout:
                t.Logf("Timed out after %d iterations\n", i);
                if i < 2 {
                    t.Error("Iterated less than 2 times!");
                }
                break foloop;
            default:
            }
            pool.Add(path, &Event{Path: path, Time: time.Now()});
        }
        time.Sleep(time.Millisecond*400); //Wait for it to free all events
        count:=0;
        lastloop:
        for{
            select{
            case <-events:
                count++;
            default:
                break lastloop;
            }
        }
        t.Logf("Finished with %d events\n", count);
        return count;
    }
    err:=quick.CheckEqual(testFn, alwaysInt(1), eventWaitConfig);
    if err!=nil{
        t.Error(err);
    }
}

func BenchmarkEventWaitPool(b *testing.B){
    pool:=NewEventWaitPool(make(chan *Event, 3), 1000);
    path:="a file path";
    b.ResetTimer();
    for i:=0; i<b.N; i++{
        pool.Add(path, &Event{Path: path, Time: time.Now()});
    }
}

func TestMutexedLinkedList(t *testing.T){
    const numEntries=2000;
    f:=func(name string) error{
        list:=&MutexedLinkedList{};
        for i:=0; i<numEntries; i++{
            list.Put(name+string(i));
        }
        if list.Len() != numEntries{
            return fmt.Errorf("expected to have %d entries and got %d", numEntries, list.Len());
        }
        list.Iterate(func(_ interface{}){});
        if list.Len() > 0{
            return fmt.Errorf("expected map to be empty but still has %d elements", list.Len());
        }
        return nil;
    }
    err:=quick.CheckEqual(f, alwaysNilString, mutexedListConfig);
    if err!=nil{
        if e,ok:=err.(*quick.CheckEqualError); ok {
            t.Errorf("test errored: %s\n", e.Out1);
        }else{
            t.Error(err);
        }
    }
}

func BenchmarkMutexedLinkedList(ob *testing.B){
    putTest:=func(b *testing.B){
        ml:=&MutexedLinkedList{};
        for i:=0; i<b.N; i++ {
            ml.Put(&Event{}); //A simple example of inserting a big object
        }
        ml=nil;
    }
    iterateTest:=func(b *testing.B){
        ml:=&MutexedLinkedList{};
        numItems:=10000;
        //Insert data into the list
        for i:=0; i<numItems; i++ {
            ml.Put(i);
        }
        b.ResetTimer(); //Now starts the real benchmark
        counter:=0;
        for i:=0; i<b.N; i++{
            ml.Iterate(func(s interface{}){
                counter++;
            })
        }
        b.StopTimer();
        ml=nil;
        runtime.GC(); //Free the allocated memory
    }
    ob.Run("Put", putTest);
    runtime.GC(); //Collect garbage from older tests
    ob.Run("Iterate", iterateTest);
}

type famTestEntry struct{
    //Type
    evtType string;
    //The entry if nil createContext will be used
    entry *contextWCancel;
    //How to create a context from a passed one. Must provide also a cancelFunc
    createContext func(context.Context) (context.Context, context.CancelFunc);
    //Whether the entry context will be cancelled by the end of the test
    cancelExpected bool;
    //Whether the entry addition must wait for other type
    waitExpected bool;
    //Whether the entry addition should panic
    panicExpected bool;
}

var famtestEntries=[][]famTestEntry{
    {
        {
            evtType: "CREATE",
            createContext: timeoutContext(time.Millisecond*500),
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
            cancelExpected: true, //Next event is a WRITE
            waitExpected: true,
        },
        {
            evtType: "WRITE",
            createContext: timeoutContext(time.Second*1), //Last context should have a timeout
            waitExpected: true, //Wait for CREATE event
        },
    },
    {
        {
            evtType: "CREATE",
            createContext: context.WithCancel,
            cancelExpected: true,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
            cancelExpected: true, //Next event is a WRITE
            waitExpected: true,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
            cancelExpected: true,
            waitExpected: true, //Wait for CREATE event
        },
        {
            evtType: "CHMOD",
            createContext: context.WithCancel,
            cancelExpected: true,
            waitExpected: true, //Wait for CREATE event
        },
        {
            evtType: "REMOVE",
            createContext: timeoutContext(time.Millisecond*500),
        },
    },
    {
        {
            evtType: "REMOVE",
            createContext: timeoutContext(time.Millisecond*1),
            cancelExpected: true,
        },
        {
            evtType: "CREATE",
            createContext: timeoutContext(time.Millisecond*2),
            panicExpected: true,
        },
    },
}

var famTestData=[][]famTestEntry{
    {
        {
            evtType: "CREATE",
            createContext: timeoutContext(time.Millisecond*500),
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: timeoutContext(time.Second*1), //Last context should have a timeout
        },
    },
    {
        {
            evtType: "CREATE",
            createContext: timeoutContext(time.Millisecond*100),
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: timeoutContext(time.Millisecond*200),
        },
        {
            evtType: "CHMOD",
            createContext: context.WithCancel,
        },
    },
    {
        {
            evtType: "CREATE",
            createContext: timeoutContext(time.Millisecond*100),
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: context.WithCancel,
        },
        {
            evtType: "WRITE",
            createContext: timeoutContext(time.Millisecond),
        },
        {
            evtType: "CHMOD",
            createContext: context.WithCancel,
        },
        {
            evtType: "REMOVE",
            createContext: timeoutContext(time.Millisecond*500),
        },
    },
}

func TestFileActionsMap(t *testing.T){
    testFamEntries:=func (currentCase []famTestEntry) func(*testing.T){
        return func(t *testing.T){
            masterContext, masterCancel:=context.WithCancel(context.Background());
            famEntry:=&famEntry{
                entry: make(map[string]*contextWCancel),
            };
            errorChannels:=make([]chan error, 0);
            waitForCancel:=make([]context.Context, 0);
            var lastContext context.Context;
            for key,v:=range(currentCase){
                if v.entry == nil{
                    v.entry=createcwc(v.createContext(masterContext));
                }
                t.Logf("Current context address is %p", v.entry.context);
                //Perform dry run
                mustWait:=tryAdding(famEntry, v.evtType, v.entry);
                if mustWait != v.waitExpected{
                    t.Errorf("Add said event %q must wait: %t and wasn't supposed to do that", v.evtType, mustWait);
                    break;
                }
                didPanic:=tryIteratePanic(famEntry, &v);
                if didPanic!=v.panicExpected{
                    t.Errorf("Event %d(%s) said panic=%t but %t", key, v.evtType, v.panicExpected, didPanic);
                }
                //Now do it for real
                famEntry.iterateCancel(v.evtType);
                famEntry.directAdd(v.evtType, v.entry);
                go famEntry.iterateWait(v.entry.context, v.evtType); //Wait on another goroutine
                ch:=make(chan error, 1);
                if !v.cancelExpected {
                    go ensureNotCancelled(masterContext, v.entry.context, key, ch);
                }else{
                    go ensureCancelled(masterContext, v.entry.context, key, ch);
                    waitForCancel=append(waitForCancel, v.entry.context);
                }
                errorChannels=append(errorChannels, ch);
                if v.entry.context.Err() == nil {
                    lastContext=v.entry.context;
                }
            }
            timeo:=time.After(time.Second*10);
            if lastContext != nil {
                select{
                case <-lastContext.Done():
                case <-timeo:
                    t.Error("Timed out waiting for last event");
                }
            }
            //Reset the first timeout
            timeo=time.After(time.Second*10);
            t.Logf("Now waiting for %d events\n", len(waitForCancel));
            for _,v:=range(waitForCancel){
                select{
                case <-v.Done():
                case <-timeo:
                    t.Errorf("Timed out waiting for a event which would be cancelled(%p)", v);
                }
            }
            masterCancel();
            for _,ch:=range(errorChannels){
                if v,ok:=<-ch; ok{
                    t.Error(v);
                }
            }
        }
    }
    testFam:=func(t *testing.T){
        masterCtx, masterCancel:=context.WithCancel(context.Background());
        fam:=NewFileActionsMap(masterCtx);
        toWait:=make([]context.Context, 0);
        for index,bunch:=range(famTestData){
            fileId:=fmt.Sprintf("id-%d", index);
            for _,v:=range(bunch){
                ctx, cancel:=context.WithTimeout(masterCtx, time.Duration(rand.Int()%500));
                t.Logf("Registering entry %s(%s)\n", fileId, v.evtType);
                toWait=append(toWait, ctx);
                fam.Register(fileId, v.evtType, ctx, cancel);
            }
        }
        masterCancel();
        timeout:=time.After(time.Second*5);
        for _,v:=range(toWait){
            select{
            case <-timeout:
                t.Errorf("Timed out waiting");
            case <-v.Done():
            }
        }
        fam.housekeeping_thread();
        fam.mutex.RLock();
        if ln:=len(fam.mp); ln!=0{
            t.Errorf("There are %d objets in map! Maybe there's an error on the housekeeping thread", ln);
            for k,v:=range(fam.mp){
                v.mutex.Lock();
                t.Logf("Object %q, %+v\n", k, v.entry);
                for evtType,ent:=range(v.entry){
                    t.Logf("Entry %s: Error: %s", evtType, ent.context.Err());
                }
                v.mutex.Unlock();
            }
        }
        fam.mutex.RUnlock();
    }
    t.Run("FAM Entry", func(t *testing.T){
        for num,entries:=range(famtestEntries){
            t.Run(fmt.Sprintf("Case %d", num), testFamEntries(entries));
        }
    })
    t.Run("File action map", testFam);
}

func timeoutContext(timeout time.Duration) func(context.Context) (context.Context, context.CancelFunc){
    innerFunction:=func(ctx context.Context) (context.Context, context.CancelFunc){
        return context.WithTimeout(ctx, timeout);
    }
    return innerFunction;
}

func createcwc(ctx context.Context, fn context.CancelFunc) *contextWCancel{
    return &contextWCancel{
        context: ctx,
        cancelFunc: fn,
    }
}

func tryAdding(entry *famEntry, evtType string, cwc *contextWCancel) (mustWait bool){
    //Test if will have to wait
    waitFor:=0;
    entry.iterate(waitTypes[evtType], func(cwc *contextWCancel){
        waitFor++;
    })
    return waitFor!=0;
}

func tryIteratePanic(entry *famEntry, v *famTestEntry) (didPanic bool){
    entry.mutex.Lock();
    for range(entry.entry){
        defer func(){
            if err:=recover(); err!=nil{
                didPanic=true;
            }
        }();
    }
    entry.mutex.Unlock();
    entry.iteratePanic(v.evtType);
    return;
}

func ensureNotCancelled(ctx context.Context, targetContext context.Context, contextNumber int, errors chan<- error){
    defer close(errors);
    select{
    case <-ctx.Done():
    case <-targetContext.Done():
        if targetContext.Err() == context.Canceled{
            errors<-fmt.Errorf("context %d cancelled artificially", contextNumber);
        }
    }
}

func ensureCancelled(ctx context.Context, targetContext context.Context, contextNumber int, errors chan<- error){
    defer close(errors);
    select{
    case <-ctx.Done():
        errors<-fmt.Errorf("context %d not cancelled", contextNumber);
    case <-targetContext.Done():
        if ctx.Err() != nil {
            errors<-fmt.Errorf("context %d was not cancelled", contextNumber);
        }
    }
}
