package types;

import (
    "container/list"
    "context"
    "fmt"
    "sync"
    "time"
)

const(
    //In seconds
    HOUSEKEEPING_INTERVAL=120;
)

var(
    //Stores the last time FAM Housekeeping function was run
    famHousekeepingLastRun time.Time;
    //Synchronize access to variables
    varMutex=new(sync.Mutex);
)

//A simple mutexed map to check if a key exists
type MutexedBlacklist struct {
    mutex *sync.RWMutex
    list  map[string]*struct{}
}

func NewMutexedBlacklist() *MutexedBlacklist {
    return &MutexedBlacklist{
        mutex: new(sync.RWMutex),
        list:  make(map[string]*struct{}),
    }
}

//Get value contained in blacklist. If exists is false, the condition is nil
func (mb *MutexedBlacklist) Get(key string) (exists bool) {
    mb.mutex.RLock()
    defer mb.mutex.RUnlock()
    _, exists = mb.list[key]
    return
}

//Add a *sync.Cond to map under key. If it already exists, error won't be nil and value will have the current value
func (mb *MutexedBlacklist) Add(key string) (err error) {
    mb.mutex.Lock()
    defer mb.mutex.Unlock()
    if _, exists := mb.list[key]; exists {
        err = NewEntryExistsError(key)
    } else {
        mb.list[key] = nil
    }
    return
}

//Remove will unlock
func (mb *MutexedBlacklist) Remove(key string) error {
    mb.mutex.Lock()
    defer mb.mutex.Unlock()
    if _, exists := mb.list[key]; exists {
        delete(mb.list, key)
    } else {
        return NewEntryNotFoundError(key)
    }
    return nil
}

//This map has conditions to be resolved after a file has been created
//It's safe to be used by multiple goroutines. Use NewResolveMap to initialize
type ResolveMap struct {
    mp    map[string]context.Context
    mutex sync.RWMutex
    ctx context.Context;
}

//Generate a ResolveMap
func NewResolveMap(ctx context.Context) *ResolveMap {
    cm := &ResolveMap{
        mp: make(map[string]context.Context),
        ctx: ctx,
    }
    //Start the housekeeping goroutine
    go cm.housekeepingGoroutine();
    return cm
}

//Check if a key exists on the ResolveMap and return the inside context if present
//The context may exist and be finalized.
//
//It's safe to check the exists bool as it will be false also if the context exists and it's already cancelled.
func (cm *ResolveMap) Check(key string) (mapContext context.Context, exists bool) {
    cm.mutex.RLock()
    defer cm.mutex.RUnlock()
    mapContext, exists = cm.mp[key]
    exists=mapContext!=nil&&mapContext.Err()==nil; //Context may exist and be finalized so it doesn't exist
    return
}

//Wait for a map key. Use the context to be able to interrupt the operation
//The error this function returns doesn't mean it has failed, it's the error reported by context
//which is populated after it was cancelled.
func (cm *ResolveMap) Wait(ctx context.Context, key string) (exists bool, err error) {
    mapContext, exists := cm.Check(key)
    if !exists {
        return
    }
    select {
    //Go doesn't fall trough to the next element, so this is safe
    case <-mapContext.Done():
    case <-ctx.Done():
        err = ctx.Err()
    }
    return
}

//Add a context to the map. Will be removed when done by the housekeeping thread
func (cm *ResolveMap) Add(key string, ctx context.Context) (err error) {
    old, exists := cm.Check(key)
    if exists && old.Err() == nil{ //We should ignore nil contexts
        err = NewEntryExistsError(key)
    } else if ctx == nil {
        err = fmt.Errorf("context must not be nil while adding %s to condition map", key)
    }
    if err != nil {
        return
    }
    cm.mutex.Lock()
    defer cm.mutex.Unlock()
    cm.mp[key] = ctx
    return
}

//This goroutine ensures the map has no stale objets (canceled contexts)
func (cm *ResolveMap) housekeepingGoroutine(){
    ticker:=time.NewTicker(time.Second*10);
    loop:
    for range(ticker.C){
        cm.removeStaleObjects();
        select{
        case <-cm.ctx.Done():
            break loop;
        default:
        }
    }
    ticker.Stop();
}

func (cm *ResolveMap) removeStaleObjects(){
    cm.mutex.Lock();
    defer cm.mutex.Unlock();
    for key,v:=range(cm.mp){
        if v.Err() != nil{
            delete(cm.mp, key);
        }
    }
}

//A simple map holding events which are waiting to be released
//It holds events until a timer has fired. Then they're released to the sender channel
type EventWaitPool struct {
    //A map holding the *Event struct
    mp       map[string]*Event
    mutex    sync.RWMutex
    sender   chan<- *Event
    timeout  int64
    tickTime time.Duration
}

func NewEventWaitPool(sender chan<- *Event, timeout int) *EventWaitPool {
    return &EventWaitPool{
        mp:       make(map[string]*Event),
        sender:   sender,
        timeout:  int64(timeout),
        tickTime: time.Millisecond * 100, //Check every 100ms
    }
}

//Add a file to the pool. Does nothing if the file already exists
//After the timeout is fired, the event is sent
func (wp *EventWaitPool) Add(path string, event *Event) {
    if event.Time.IsZero() {
        panic("Time can't be 0 when adding to EventWaitPool")
    }
    wp.mutex.Lock()
    defer wp.mutex.Unlock()
    //Use the mutex to check for duplicates
    if evt, ok := wp.mp[path]; ok {
        *evt = *event //Update event
        return
    }
    //Must be sent after delay
    wp.mp[path] = event
    go func() {
        ticker := time.NewTicker(wp.tickTime)
        var sent bool
        for range ticker.C {
            wp.mutex.Lock()
            if time.Since(wp.mp[path].Time).Milliseconds() >= wp.timeout {
                wp.sender <- wp.mp[path]
                delete(wp.mp, path)
                ticker.Stop()
                sent = true
            }
            wp.mutex.Unlock()
            if sent {
                break
            }
        }
    }()
}

//A safe linked list for concurrent access.
//The initial value is a list ready to use.
type MutexedLinkedList struct {
    list  list.List
    mutex sync.Mutex
}

//A wrapper for List.Init()
func (mll *MutexedLinkedList) Init() {
    mll.mutex.Lock()
    defer mll.mutex.Unlock()
    mll.list.Init()
}

func (mll *MutexedLinkedList) Put(element interface{}) {
    mll.mutex.Lock()
    defer mll.mutex.Unlock()
    mll.list.PushBack(element)
}

func (mll *MutexedLinkedList) Iterate(callback func(interface{})) {
    defer mll.Init() //Reset list after all elements are read
    mll.mutex.Lock()
    defer mll.mutex.Unlock()
    for el := mll.list.Front(); el != nil; el = el.Next() {
        callback(el.Value)
    }
}

func (mll *MutexedLinkedList) Len() int {
    mll.mutex.Lock()
    defer mll.mutex.Unlock()
    return mll.list.Len()
}

//Type for FileActionsMap. Holds a context and it's cancel function
type contextWCancel struct {
    context    context.Context
    cancelFunc context.CancelFunc
}

//A single entry for FileActionsMap
type famEntry struct {
    entry map[string]*contextWCancel
    mutex sync.Mutex
}

//Add a waiting type to map
func (entry *famEntry) addType(evtType string, cwc *contextWCancel) {
    entry.iteratePanic(evtType)
    entry.iterateCancel(evtType)
    entry.directAdd(evtType, cwc)
    entry.iterateWait(cwc.context, evtType) //No event waits for itself so wait must be after add
}

//Add a entry to map w/o more walks
func (entry *famEntry) directAdd(evtType string, cwc *contextWCancel) {
    //Lock map to prevent concurrent access
    entry.mutex.Lock()
    defer entry.mutex.Unlock()
    //If the current entry exists, must cancel it and readquire the lock.
    //This unlock allows the remove goroutine to adquire the lock (if it's fast enough) before we override the value
    if v,exists:=entry.entry[evtType]; exists{
        v.cancelFunc();
        entry.mutex.Unlock();
        <-v.context.Done();
        entry.mutex.Lock();
    }
    //Add to map after iterations
    entry.entry[evtType] = cwc
    go func() { //Remove entry when done
        <-cwc.context.Done()
        entry.mutex.Lock()
        defer entry.mutex.Unlock()
        //Entry maybe was overriden, it it was, we **must not** remove it again
        if v,exists:=entry.entry[evtType]; exists && v==cwc{
            delete(entry.entry, evtType)
        }
    }()
}

//Iterate the waiting types looking for panic conditions
func (entry *famEntry) iteratePanic(evtType string) {
    entry.iterate(panicTypes[evtType], func(cwc *contextWCancel) {
        panic(fmt.Errorf("undesired state of map on %q event: %+v", evtType, entry.entry))
    })
}

//Iterate the waiting types cancelling all the cancellable events there
func (entry *famEntry) iterateCancel(evtType string) (cancelled int) {
    cancelled = entry.iterate(cancelTypes[evtType], func(cwc *contextWCancel) {
        cwc.cancelFunc()
    })
    return
}

//Iterate the waiting types waiting for all the waitable types there
func (entry *famEntry) iterateWait(me context.Context, evtType string) (wait int) {
    contextsToWait := make([]context.Context, 0)
    wait = entry.iterate(waitTypes[evtType], func(cwc *contextWCancel) {
        contextsToWait = append(contextsToWait, cwc.context)
    })
loop:
    for _, ctx := range contextsToWait {
        select {
        case <-ctx.Done():
        case <-me.Done():
            break loop
        }
    }
    return
}

//Generic iterate
func (entry *famEntry) iterate(types []string, callback func(*contextWCancel)) (iterated int) {
    entry.mutex.Lock()
    defer entry.mutex.Unlock()
    for _, entryName := range types {
        if cwc, exists := entry.entry[entryName]; exists {
            callback(cwc)
            iterated++
        }
    }
    return
}

//Cancel and Wait types for each event
var (
    cancelTypes = map[string][]string{
        "CREATE": {
            "CREATE",
        },
        "REMOVE": {
            "CREATE",
            "REMOVE",
            "WRITE",
            "CHMOD",
        },
        "WRITE": {
            "WRITE",
        },
        "CHMOD": {
            "CHMOD",
        },
    }
    waitTypes = map[string][]string{
        "CREATE": {
            "WRITE",
            "CHMOD",
        },
        "WRITE": {
            "CREATE",
            "CHMOD",
        },
        "CHMOD": {
            "CREATE",
            "WRITE",
        },
    }
    panicTypes = map[string][]string{
        "CREATE": {
            "REMOVE",
        },
        "WRITE": {
            "REMOVE",
        },
        "CHMOD": {
            "REMOVE",
        },
    }
)

//A simple map to handle concurrent actions over the same file
//It's thread safe
type FileActionsMap struct {
    globalContext context.Context
    mp map[string]*famEntry
    mutex sync.RWMutex
}

//Create a new FileActionsMap with the specified context as root for events w/o their own context
func NewFileActionsMap(ctx context.Context) *FileActionsMap {
    return &FileActionsMap{
        globalContext: ctx,
        mp: make(map[string]*famEntry),
    }
}

//Register an event and wait if needed.
//This function may return an error if the context or cancelFunc were nil or the context error.
//There isn't any return value. There are 4 possible ways
// - The event is registered and the function returns
// - The function blocks until a event is done
// - Other context is cancelled and the function continues
// - A combination of the previous 2
func (fam *FileActionsMap) Register(fileId, evtType string, ctx context.Context, cancelFunc context.CancelFunc) error{
    fam.mutex.RLock()
    entry, exists := fam.mp[fileId]
    fam.mutex.RUnlock()
    if ctx==nil || cancelFunc==nil{
        return NewArgumentsError("Context nor CancelFunc can be nil in call to FAM Register");
    }
    cwc := &contextWCancel{ctx, cancelFunc}
    if !exists { //First possibility
        entry=fam.add(fileId, evtType, cwc)
    }
    entry.addType(evtType, cwc)
    fam.housekeeping();
    return ctx.Err(); //The context maybe was cancelled. Tell that.
}

func (fam *FileActionsMap) add(fileId, evtType string, cwc *contextWCancel) *famEntry{
    fam.mutex.Lock()
    defer fam.mutex.Unlock()
    entry := &famEntry{
        entry: make(map[string]*contextWCancel),
    }
    fam.mp[fileId] = entry
    return entry;
}

//Remove empty slots from map so we can free memory. Should be called on a secondary goroutine
func (fam *FileActionsMap) housekeeping(){
    varMutex.Lock();
    defer varMutex.Unlock();
    if time.Since(famHousekeepingLastRun).Seconds() < HOUSEKEEPING_INTERVAL{
        return;
    }
    famHousekeepingLastRun=time.Now();
    go fam.housekeeping_thread();
}

func (fam *FileActionsMap) housekeeping_thread(){
    fam.mutex.Lock();
    defer fam.mutex.Unlock();
    for key,entry:=range(fam.mp){
        entry.mutex.Lock();
        var removeEntry bool;
        if len(entry.entry)==0{
            removeEntry=true;
        }else{
            removeEntry=objectsHaveFinished(entry.entry);
        }
        if removeEntry{
            delete(fam.mp, key);
        }
        entry.mutex.Unlock();
    }
}

func objectsHaveFinished(entry map[string]*contextWCancel) bool{
    for _,v:=range(entry){
        if v.context.Err()!=nil{
            return true;
        }
    }
    return false;
}
