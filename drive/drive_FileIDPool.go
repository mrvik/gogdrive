package drive;

import(
    "sync"
)

const(
    MIN_FID_POOL_SIZE=10;
    OK_FID_POOL_SIZE=20;
)

//This struct holds the FileID pool
type FileIDPool struct{
    mutex *sync.Mutex;
    pool []string;
}

//Generate a new FileIDPool and populate it with OK_FID_POOL_SIZE
func NewFileIDPool() (pool *FileIDPool, err error){
    initialPool,err:=callGetIDS(OK_FID_POOL_SIZE);
    if err != nil{
        return;
    }
    pool=&FileIDPool{
        mutex: new(sync.Mutex),
        pool: initialPool.Ids,
    }
    return;
}

//Get a FileID. The pool should have at least an ID available
//If no ID is available, will hold until Drive generates some IDs and we fill the pool
func (fp *FileIDPool) GetFileID() (fid string, err error){
    fp.mutex.Lock();
    defer fp.mutex.Unlock();
    if len(fp.pool) < 1{
        fp.mutex.Unlock();
        err=fp.refillPool(1);
        log.Debugf("FileID pool exhausted. Pulling more IDs");
        fp.mutex.Lock();
    }
    if err != nil{
        return;
    }
    fid, fp.pool=fp.pool[0], fp.pool[1:];
    go fp.conditionalRefill();
    return;
}

func (fp *FileIDPool) refillPool(addToCount int) error{
    fp.mutex.Lock();
    defer fp.mutex.Unlock();
    pullCount:=OK_FID_POOL_SIZE-len(fp.pool)+addToCount;
    log.Debugf("Pulling %d ids from Drive\n", pullCount);
    ids,err:=callGetIDS(int64(pullCount));
    if err != nil{
        return err;
    }
    fp.pool=append(fp.pool, ids.Ids...);
    return nil;
}

func (fp *FileIDPool) conditionalRefill(){
    fp.mutex.Lock();
    if remain:=len(fp.pool); remain <= MIN_FID_POOL_SIZE{
        fp.mutex.Unlock();
        err:=fp.refillPool(0);
        if err != nil{
            log.Warnf("Error while refilling FID pool: %s\n", err);
        }
    }else{
        fp.mutex.Unlock();
    }
}
