package drive;

import(
    "bufio"
    "fmt"
    "time"
    "path"
    "sync"
  . "gitlab.com/mrvik/logger"
  . "gitlab.com/mrvik/gogdrive/types"
    "gitlab.com/mrvik/gogdrive/lockfile"
    "gitlab.com/mrvik/gogdrive/util"
    "gitlab.com/mrvik/gogdrive/convert"
    "net/http"
  drv "google.golang.org/api/drive/v3"
    "google.golang.org/api/option"
    "google.golang.org/api/googleapi"
    "golang.org/x/net/context"
    "reflect"
)

const(
    DRIVE_FILE_FIELDS="id,name,mimeType,trashed,permissions,parents,webContentLink,viewedByMeTime,createdTime,modifiedTime,ownedByMe,size";
)

var(
    //Holds the mandatory fields to get from drive
    DRIVE_FIELDS=[]googleapi.Field{"nextPageToken", googleapi.Field(fmt.Sprintf("files(%s)", DRIVE_FILE_FIELDS))};
    log Logger;
    wg *sync.WaitGroup;
    drvreq <-chan *Request;
    evchan chan<- *Event;
    config Config;
    svc *drv.Service;
    conversionTree *convert.Tree;
    rootID, permissionID string;
    fidPool *FileIDPool;
)

func Init(ctx context.Context, lg Logger, cnfp *Config, client *http.Client, wtg *sync.WaitGroup, reqchan chan *Request, evtch chan<- *Event) error{
    var err error;
    log=lg;
    config=*cnfp;
    wg=wtg;
    drvreq=reqchan;
    evchan=evtch;
    state,err:=lockfile.NewState(cnfp);
    if err != nil {
        return err;
    }
    svc,err=drv.NewService(ctx, option.WithHTTPClient(client));
    if err != nil {
        return err;
    }
    fl,err:=getFileList();
    if err != nil {
        return err;
    }
    fidPool, err=NewFileIDPool();
    if err != nil {
        return err;
    }
    rootID,permissionID=getRootID();
    convert.Init(lg);
    conversionTree,err=convert.NewTree(rootID, fl);
    if err != nil {
        return err;
    }
    //Create channel listeners
    wg.Add(2);
    go watchChanges(ctx, state);
    go watchDriveEvents(ctx);
    return err;
}

//Get the permission id and root ID
func getRootID() (string,string){
    call:=svc.Files.Get("root"); //Root is an alias for the Root ID, but only Drive knows atm
    call.Fields(DRIVE_FILE_FIELDS); //Skip the first element
    f,err:=call.Do();
    if err != nil {
        panic(err);
    }
    var permissionId string;
    for _,p:=range(f.Permissions) {
        if p.Role=="owner" {
            permissionId=p.Id;
        }
    }
    if permissionId == "" {
        panic("Can't find the user's permission ID");
    }
    return f.Id, permissionId;
}

//Start watching Drive changes. Takes a state parameter to get the start page token
//Updates the lockfile when a new start page token is received
func watchChanges(ctx context.Context, state *lockfile.State){
    var exit bool;
    defer wg.Done();
    sptoken:=state.StartPageToken;
    if sptoken == "" {
        var err error;
        sptoken,err=getStartPageToken();
        if err != nil {
            panic(err);
        }
    }
    ticker:=time.NewTicker(2*time.Second);
    defer ticker.Stop();
    for !exit {
        select {
        case <-ctx.Done():
            exit=true;
        case <-ticker.C:
            changes,newtoken,err:=getChangesFrom(sptoken);
            if err != nil {
                log.Errorf("Error getting changes from drive: %s\n", err);
            }else if len(changes) > 0 {
                err=sendChangesAsEvents(changes); //Events souldn't be sent if there's an error
                if err != nil {
                    panic(fmt.Sprintf("Error processing changes: %s\n", err));
                } else {
                    //Save token
                    sptoken=newtoken;
                    err=state.UpdateSPToken(newtoken);
                    if err != nil {
                        panic(err);
                    }
                }
            }
        }
    }
}

//Convert changes to events and send them via channel
func sendChangesAsEvents(changes []*drv.Change) error{
    errors:=make([]error, 0);
    for _,change:=range(changes) {
        event,err:=generateEventFromChange(change);
        if err != nil {
            log.Errorf("Error while creating event from remote change: %s\n", err);
            errors=append(errors, err);
        }else if event != nil{
            log.Infof("Remote event: %+v\n", *event); //FIXME Debug only
            if event.Propagate {
                evchan<-event;
            }
        }
    }
    if len(errors) == len(changes) {
        return NewTooManyErrors(len(errors), len(changes));
    }
    return nil;
}

func generateEventFromChange(change *drv.Change) (*Event, error){
    evtype:="WRITE"; //Write event by default
    file:=change.File; //File may not be present if resource was removed
    onTree,_:=conversionTree.GetFileByID(change.FileId); //May be nil. Ignore error
    resultingEvent:=&Event{
        Origin: 1,
        Propagate: true,
        File: file,
        FileID: change.FileId,
    };
    tm,err:=time.Parse(time.RFC3339, change.Time);
    if err == nil {
        resultingEvent.Time=tm;
    }else{
        log.Warnf("Can't parse change time: %s\n", err);
    }
    if change.Removed {
        evtype="REMOVE";
        if onTree == nil && file == nil {
            log.Warn(NewShadowFileError(change.FileId).Error()); //File is not on the change nor on the conversion tree, so there's nothing to remove
            return nil,nil; //Not really an error
        }
        if resultingEvent.File == nil { //It should
            if onTree != nil {
                resultingEvent.File=onTree.File; //Get the file on the tree
                resultingEvent.Path=onTree.Path;
            }
        }
    } else if file != nil {
        ok:=filterFile(file, "event");
        if !ok {
            return nil,nil; //Not an error, but event shouldn't be fired
        }
        if file.Trashed || len(file.Parents) < 1 {
            //Remove the file. If file has no parents shouldn't be here
            if onTree != nil {
                resultingEvent.File = onTree.File; //File on struct should be the one on our end, not a new one.
                resultingEvent.Path = onTree.Path;
                resultingEvent.OldPath = onTree.Path; //Path and OldPath are the same. Technically, on a remotion event there's no Path, so you should use the OldPath
            } else {
                log.Warn(NewShadowFileError(change.FileId).Error()); //File is not on our hierarchy, so it didn't get into the local filesystem.
                return nil,nil; //Not really an error
            }
            evtype="REMOVE";
        }else if onTree != nil {
            oldDate:=onTree.ModifiedTime;
            newDate:=file.ModifiedTime;
            if onTree.File.Name != file.Name || !reflect.DeepEqual(onTree.File.Parents, file.Parents) {
                evtype="RENAME"; //You can't move a file and modify it's content at the same time
                resultingEvent.OldPath=onTree.Path; //Read the next comment before adding a new line.
                //Increment here the number of attempts to write resultingEvent.Path= and then removed the line -> 2
                //New path will be generated on conversionTree after ConsumeEvent. The next functions consuming this event will check for path to exist.
            }else if oldDate == newDate { //Dates are equal, file not modified (I swear)
                return nil,nil;
            }
        }else{ //Not on the tree, so this is an addition
            evtype="CREATE";
        }
    }
    //In case of error should return before this like. The result will be returned now without error
    resultingEvent.Type=evtype;
    if evtype == "" {
        return nil, NewArgumentsError("Event type not defined, but reached end of function. Please debug me");
    }
    ctErr:=conversionTree.ConsumeEvent(resultingEvent); //Prior to releasing the event to the calling funcion
    if ctErr!=nil{ //The error ends here. It isn't fatal. Fatal errors do panic
        log.Warnf("Error consuming event on conversion tree: %s\n", ctErr);
    }
    return resultingEvent,nil;
}

//This function takes in account config options and says wether a file can be included or not on events or file listing
func filterFile(file *drv.File, on string) bool{
    if on != "event" {
        if file.Trashed {
            return false;
        }
    }
    if mime:=file.MimeType; util.IsGoogleApp(mime) && !util.IsDir(mime) { //Check if file is a Google Apps document
        if !config.Drive.AllowDriveFiles { //Allow you to shoot your foot
            return false;
        }
    }
    if file.OwnedByMe { //No permission check is needed
        return true;
    }else if config.Drive.AllowSharedFiles {
        return hasWritePermission(file.Permissions);
    }
    //JDrive had a method to add shared files to the local filesystem on the Root folder, but then we discovered that you can add a shared file to your drive, then GoGDrive will sync it
    return false; //By default
}

//Return true or false depending on the current user has write permission on a file
func hasWritePermission(permissions []*drv.Permission) bool{
    var so bool;
    for _,perm:=range(permissions){
        if perm.Id == permissionID {
            switch perm.Role {
            case "owner", "writer":
                so=true;
            }
        }
    }
    return so;
}

//Get a list of changes from a start token to the actual moment.
//The function returns the next start page token. If no changes are made, the returned token will be the same and the changes list will be empty
//If an error ocurrs, both changes and npt will be nil and empty respectively.
func getChangesFrom(token string) ([]*drv.Change,string,error){
    var (
        rt []*drv.Change;
        npt string;
    );
    if token == "" {
        return nil,"",NewArgumentsError("Start page token cannot be empty");
    }
    ctoken:=token;
    for ctoken != "" {
        call:=svc.Changes.List(ctoken);
        call.Fields("*");
        clist,err:=call.Do();
        if err != nil {
            return nil,"",err;
        }
        npt,rt=clist.NewStartPageToken,append(rt, clist.Changes...);
        ctoken=clist.NextPageToken;
    }
    return rt,npt,nil;
}

//Returns a start page token got from drive
func getStartPageToken() (string,error){
    call:=svc.Changes.GetStartPageToken()
    token,err:=call.Do();
    return token.StartPageToken,err;
}

//Get file list to create the conversion tree
func getFileList() ([]*drv.File,error){
    rt:=make([]*drv.File, 0);
    var npt string;
    for {
        call:=svc.Files.List();
        call.Fields(DRIVE_FIELDS...).PageSize(100).Q("trashed!=true");
        if npt != "" {
            call.PageToken(npt);
        }
        page,err:=call.Do();
        if err != nil {
            return nil,err;
        }
        rt=append(rt, page.Files...);
        if npt=page.NextPageToken; npt == "" {
            break;
        }
    }
    for index,file:=range(rt) {
        var rq *Request;
        if *config.Drive.RemoveOrphans && len(file.Parents)<1 {
            log.Warnf("Removing orphaned file %s with ID %s\n", file.Name, file.Id);
            rq=NewRequest("remove", file.Id);
            removeFile(rq);
            rt[index]=nil;
        }else if !filterFile(file, "listing") {
            rt[index]=nil;
        }
    }
    return rt,nil;
}

func watchDriveEvents(ctx context.Context){
    var exit bool;
    defer wg.Done();
    for !exit {
        select{
        case <-ctx.Done():
            exit=true;
        case v,ok:=<-drvreq:
            if !ok {
                panic("Drive requests channel closed!");
            }
            go processRequest(v);
        }
    }
}

//Process a given request from reqchan
func processRequest(req *Request){
    wg.Add(1);
    defer wg.Done();
    switch req.Type {
    case "get":
        getFile(req);
    case "create":
        createFile(req);
    case "update":
        updateFile(req, false); //Don't skip body
    case "updateTimestamp":
        updateFile(req, true);
    case "move":
        updateFile(req, true);
    case "remove":
        removeFile(req);
    case "metadata":
        getMetadata(req);
    case "statPath":
        stat(req, false); //False, it's a path
    case "stat":
        stat(req, true); //True, it's a FileID
    case "getFID":
        getFid(req);
    default:
        req.Status<-NewUnimplementedType(req.Type);
    }
}

//Get a file (with it's body) and write to a io.Writter passed on request.
//Here the Path is used to carry the FileID (it's the path inside Drive).
func getFile(req *Request){
    id:=req.Path;
    writer:=req.File;
    if id=="" || writer==nil{
        req.Status<-NewArgumentsError("ID was empty or writer nil on get file request");
        return;
    }
    call:=svc.Files.Get(id);
    response,err:=call.Download();
    if err != nil {
        req.Status<-err;
        return;
    }
    //We could use a buffered reader or a buffered writer. It depends on the connection speed, the disk speed and system memory.
    //But on the end this won't speed up the transfer.
    //A more fault tolerant behavior is to have a buffer on the local side to prevent a slowdown on the disk cause a transfer problem.
    buffer:=bufio.NewWriter(writer);
    body:=response.Body;
    defer writer.Close();
    defer body.Close();
    _,err=buffer.ReadFrom(body);
    if err != nil {
        req.Status<-err;
    }else{
        req.Status<-buffer.Flush();
    }
}

//Get metadata from FileID. Path field is used as FileID as it's the path on Google Drive
func getMetadata(req *Request){
    id:=req.Path;
    if id=="" {
        req.Status<-NewArgumentsError("ID was nil when passed to getMetadata call");
        return;
    }
    call:=svc.Files.Get(id);
    call.Fields(DRIVE_FILE_FIELDS);
    res,err:=call.Do();
    req.ReturnValue=res;
    req.Status<-err;
}

//Send *types.File with matching path. hasFileId indicates if req.Path holds a FileID or not.
func stat(req *Request, hasFileId bool){
    if req.Path == "" {
        req.Status<-NewArgumentsError("Path was '' when passed to statPath call");
        return;
    }
    var (
        rt *File; //Declare a variable to use type checking
        err error
    );
    if hasFileId {
        rt,err=conversionTree.GetFileByID(req.Path); //Take req.Path as FileID
    }else{
        rt,err=conversionTree.GetFileByPath(req.Path);
    }
    req.ReturnValue=rt;
    req.Status<-err;
}

//Create file and upload it. Directory is autodetected
func createFile(req *Request){
    if req.File == nil {
        req.Status<-NewArgumentsError("File not supplied when calling createFile");
        return;
    }
    if req.Path == "" {
        req.Status<-NewArgumentsError("Path was '' when passed to createFile call");
        return;
    }
    defer req.File.Close();
    patchedFile,_,_,err:=generatePatch(req, nil);
    if err != nil {
        req.Status<-err;
        return;
    }
    call:=svc.Files.Create(patchedFile);
    if !util.IsDir(patchedFile.MimeType) {
        buf:=bufio.NewReader(req.File); //Use a buffered reader to read file
        call.Media(buf); //Use the buffer as media
        if req.Context != nil {
            call.Context(req.Context);
        }
    }
    call.Fields(DRIVE_FILE_FIELDS);
    file,err:=call.Do();
    if err != nil{
        req.Status<-err;
        return;
    }
    var nfile string;
    var cterr error;
    if patchedFile.Id != "" {
        nfile,cterr=conversionTree.Update(file, true);
    }else{
        nfile,cterr=conversionTree.Add(NewFile(file, "")); //Path can be empty as conversionTree will generate a path
    }
    if cterr!=nil{
        log.Warn(cterr.Error());
    }
    req.ReturnValue=nfile;
    req.Status<-err;
}

//Update a file from a reader. Second parameter set to true skips body (update metadata only)
func updateFile(req *Request, skipBody bool){
    if req.Path == "" {
        req.Status<-NewArgumentsError("Path was '' when passed to updateFile call");
        return;
    }
    if !skipBody && req.File == nil {
        req.Status<-NewArgumentsError("File not supplied when calling updateFile");
        return;
    }
    if req.File != nil {
        defer req.File.Close();
    }
    original,err:=conversionTree.GetFileByID(req.Path); //Despite the name, it holds the FileID
    if err != nil {
        req.Status<-err;
        return;
    }
    patchedFile,addParents,removeParents, err:=generatePatch(req, original.File);
    if err != nil {
        req.Status<-err;
        return;
    }
    if !skipBody && util.IsDir(patchedFile.MimeType) { //Don't check MIME if skipBody is already true
        skipBody=true; //Skip reading file
    }
    parentsChanged:=addParents!=""||removeParents!="";
    call:=svc.Files.Update(req.Path, patchedFile);
    if parentsChanged {
        call.AddParents(addParents);
        call.RemoveParents(removeParents);
    }
    call.Fields(DRIVE_FILE_FIELDS);
    if !skipBody {
        buf:=bufio.NewReader(req.File);
        call.Media(buf);
        if req.Context != nil{
            call.Context(req.Context);
        }
    }
    updatedFile,err:=call.Do();
    if err != nil {
        req.Status<-err;
        return;
    }
    req.ReturnValue=updatedFile;
    _,err=conversionTree.Update(updatedFile, parentsChanged||req.Type=="move"); //Regenerate path if parents changed
    req.Status<-err;
}

//Remove a file from the remote drive. Attends to Config.Drive.DeleteForever to switch between Calling update or Deleting it
//Here, the Path is the FileID to be removed
func removeFile(req *Request){
    if req.Path == "" {
        req.Status<-NewArgumentsError("Path was '' when passed to removeFile call");
        return;
    }
    if config.Drive.DeleteForever {
        call:=svc.Files.Delete(req.Path);
        err:=call.Do();
        req.Status<-err;
    }else{
        patchedFile,_,_,err:=generatePatch(req, nil);
        if err != nil {
            req.Status<-err;
            return;
        }
        patchedFile.Trashed=true;
        call:=svc.Files.Update(req.Path, patchedFile);
        _,err=call.Do();
        req.Status<-err;
    }
}

//Take a fileID from the pool and return it
func getFid(req *Request){
    fid,err:=fidPool.GetFileID();
    if err == nil {
        if req.Path != "" {
            err=conversionTree.RegisterIDForPath(fid, req.Path);
        }
    }
    req.ReturnValue=fid;
    req.Status<-err;
}

//Call drive.files.generateIds and return the result
func callGetIDS(numIds int64) (*drv.GeneratedIds, error){
    call:=svc.Files.GenerateIds();
    call.Count(numIds);
    res,err:=call.Do();
    return res,err;
}

//Generate a patched File resource for drive from the original and Request type and arguments.
//Also output the parent IDs to be added or removed
func generatePatch(request *Request, original *drv.File) (patchedFile *drv.File, newParents, oldParents string, rterr error) {
    patchedFile=&drv.File{}
    if request.Type=="move"{
        oldP,newP:=request.Arguments[0], request.Arguments[1];
        patchedFile.Name=path.Base(request.Path);
        if oldP != newP {
            oldParents, newParents = oldP, newP;
        }
    } //Allow this to update time and MIME
    if request.Type=="create" {
        patchedFile.Name=path.Base(request.Path);
        patchedFile.Id=request.Arguments[0];
        patchedFile.Parents,rterr=conversionTree.GenerateParents(request.Path);
        if rterr != nil {
            return;
        }
    }
    if request.File != nil {
        file:=*request.File;
        stat,err:=file.Stat();
        if err == nil {
            //Beware the atime may not be accurate as some systems enable the noatime flag
            if config.Drive.UpdateAtime {
                patchedFile.ViewedByMeTime=util.GetAtime(stat).Format(time.RFC3339);
            }
            patchedFile.ModifiedTime=stat.ModTime().Format(time.RFC3339); //Set modified time
            if stat.IsDir() {
                patchedFile.MimeType=util.DRIVE_FOLDER_MIME;
            } //Else, Drive autodetects the MIME Type
        }else{
            log.Warn(err.Error()); //Only a warning
        }
    }
    return;
}
