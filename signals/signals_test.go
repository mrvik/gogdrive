package signals;

import (
    "context"
    "gitlab.com/mrvik/logger/log"
    "os"
    "sync"
    "syscall"
    "testing"
    "time"
)

func TestInit(t *testing.T){
    //SIGTERM and SIGINT should cause the close channel to be closed.
    wg:=new(sync.WaitGroup);
    wg.Add(1); //Fake the wait group initialized on main package
    log:=log.Create("", nil, nil); //Create logger
    ctx, stop:=context.WithCancel(context.Background());
    go Init(ctx, stop, wg, log);
    time.Sleep(time.Second); //Wait until starts watching signals
    err:=syscall.Kill(os.Getpid(), syscall.SIGTERM);
    if err != nil {
        t.Fatalf("Error sending signal: %s\n", err.Error());
    }
    timech:=time.After(time.Second*2);
    select{
    case <-ctx.Done():
        t.Log("Signal catcher exited OK");
    case <-timech:
        t.Error("Signal catcher didn't exit after interrupt signal");
    }
}
