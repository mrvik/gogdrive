package signals;

import (
    "context"
  . "gitlab.com/mrvik/logger"
    "golang.org/x/sys/unix"
    "os"
    "os/signal"
    "sync"
);

//Start the signal watch. Reads from the stop channel. When SIGINT||SIGTERM is received, writes true to stop and closes the channel. If the channel is unbuffered will wait to be read from at least one goroutine.
//In addition, it uses the wait group
func Init(ctx context.Context, stop context.CancelFunc, wg *sync.WaitGroup, log Logger){
    var exit bool;
    defer stop();
    defer wg.Done();
    sigs:=make(chan os.Signal, 1);
    signal.Notify(sigs, unix.SIGINT, unix.SIGTERM);
    for !exit {
        select{
        case <-ctx.Done():
            exit=true;
        case sig,ok:=<-sigs:
            if !ok {
                log.Error("Can't read from signal channel. Channel closed!");
            }
            switch sig {
            case unix.SIGTERM,unix.SIGINT:
                exit=true; //Call to stop is deferred
            }
        }
    }
}
