package events

import (
    "testing"
    "gitlab.com/mrvik/logger/testAdapter"
)

//BLACKLIST TEST

//Test local blacklist
func TestLocalBlacklist(t *testing.T){
    log=testAdapter.CreateTestAdapter(t);
    testBlacklist(t, localBlacklist_add, localBlacklist_get, localBlacklist_remove);
}

//Test remote blacklist
func TestRemoteBlacklist(t *testing.T){
    log=testAdapter.CreateTestAdapter(t);
    testBlacklist(t, remoteBlacklist_add, remoteBlacklist_get, remoteBlacklist_remove);
}

//Generic blacklist test
func testBlacklist(t *testing.T, add func(string), get func(string)bool, remove func(string)){
    add("test");
    ok:=get("test");
    if !ok {
        t.Errorf("Tried to get recently added condition and got %t\n", ok);
    }
    remove("test");
    ok=get("test");
    if ok {
        t.Error("Got a channel for a non-existent blacklist file");
    }else{
        t.Log("Get a removed file from blacklist returned a nil channel. OK");
    }
}
