//Here's where the fun begins.
//This part holds the sync logic. What events should be processed on which way.
//Feel free to open a bug report. For sure this file is full of them.
package events;

import (
    "context"
    "time"
    "sync"
    "os"
  . "gitlab.com/mrvik/logger"
  . "gitlab.com/mrvik/gogdrive/types"
    "gitlab.com/mrvik/gogdrive/util"
    "golang.org/x/sync/semaphore"
)

const (
    MAX_GOROUTINES=100;
)

var (
    eventsContext context.Context;
    ioreq, drvreq chan<- *Request;
    localev, remoteev <-chan *Event;
    wg *sync.WaitGroup;
    config *Config;
    log Logger;
    //Array of requests that should be done ant it's mutex
    //This a double slice to allow passing events to be executed synchronously on the same worker, one after another
    requestSlice=make([][]*Request, 0);
    requestSliceMutex=new(sync.Mutex);
    localBlacklist=NewMutexedBlacklist();
    requestProcessingMutex=new(sync.Mutex); //Process a request at a time to prevent race conditions. When safe the current thread will free the lock
    remoteBlacklist=NewMutexedBlacklist();
    //This map holds files renamed and waiting to reappear
    renamedMap=make(map[string]*Event);
    renamedMapMutex=new(sync.RWMutex); //Sync access to the map
    //Semaphore to control number of goroutines on event handling
    eventWatcherSemaphore=semaphore.NewWeighted(MAX_GOROUTINES);
    fidCreationProgress *ResolveMap;
    fileActionsMap *FileActionsMap;
)

func Init(ctx context.Context, lg Logger, cfgp *Config, wgr *sync.WaitGroup, io, drv chan *Request, local, remote chan *Event){
    log, ioreq, drvreq, localev, remoteev=lg, io, drv, local, remote;
    config=cfgp;
    wg=wgr;
    blacklistReq:=NewRequest("registerBlacklist", "");
    blacklistReq.ReturnValue=localBlacklist; //Send pointer to blacklist in an unusual way
    ioreq<-blacklistReq;
    eventsContext=ctx;
    fidCreationProgress=NewResolveMap(ctx);
    fileActionsMap=NewFileActionsMap(ctx);
    wg.Add(1);
    go watchEvents(ctx);
    workerFarm(ctx, int(config.Drive.Workers));
}

//Watch events from local filesystem and drive. Events are different (the way to send them) so the're processed by separate functions
func watchEvents(ctx context.Context){
    defer wg.Done();
    var exit bool;
    for !exit {
        err:=eventWatcherSemaphore.Acquire(ctx, 1);
        if err != nil{
            break;
        }
        select {
        case <-ctx.Done():
            exit=true;
        case rmt,ok:=<-remoteev:
            if !ok {
                eventWatcherSemaphore.Release(1);
                panic("Can't read from remote event channel");
            }
            go func(){
                log.Debug("Processing remote event");
                appendToRequestSlice(processRemoteEvent(rmt));
                log.Debug("Ended processing remote event");
                eventWatcherSemaphore.Release(1);
            }();
        case local,ok:=<-localev:
            if !ok {
                eventWatcherSemaphore.Release(1);
                panic("Can't read from local event channel");
            }
            go func(){
                log.Debug("Processing local event");
                appendToRequestSlice(processLocalEvent(local));
                log.Debug("Ended processing local event");
                eventWatcherSemaphore.Release(1);
            }();
        }
    }
}

//Append elements to request slice from output of processRemote/LocalEvent
func appendToRequestSlice(requests []*Request, err error){
    if err != nil {
        log.Errorf("Error processing event: %s\n", err);
    }else if requests != nil {
        log.Debugf("Event transformed to %d requests:\n", len(requests));
        for _,req:=range(requests){
            log.Debugf("\t%+v\n", req);
        }
        requestSliceMutex.Lock();
        requestSlice=append(requestSlice, requests);
        requestSliceMutex.Unlock();
    }
}

//Get a remote event and output a Request
func processRemoteEvent(evt *Event) (reqs []*Request, err error){
    if evt == nil {
        return nil,nil;
    }
    requestProcessingMutex.Lock();
    defer requestProcessingMutex.Unlock(); //Can defer this call as this function won't wait for anything to show up
    if pth:=util.ChooseANonEmptyString(evt.Path, evt.OldPath); pth!="" {
        if remoteBlacklist_get(pth){ //Don't allow if blacklisted
            return nil, nil;
        }
    }
    reqs=make([]*Request, 0);
    switch evt.Type {
        case "REMOVE":{
            if evt.OldPath == "" {
                return nil, NewPathError("Undefined path for name %q and ID %q", evt.File.Name, evt.FileID); //Event.File shouldn't be nil. On drive.go it is filled
            }
            if !evt.Time.IsZero() {
                rq1:=NewRequest("stat", evt.OldPath);
                ioreq<-rq1;
                if status:=<-rq1.Status; status == nil {
                    stat,ok:=rq1.ReturnValue.(*os.FileInfo);
                    if ok && stat != nil && *stat != nil {
                        evtt,modt:=evt.Time,(*stat).ModTime();
                        if modt.After(evtt) {
                            return nil,nil; //File updated after event fired. It's a draw. If it get's updated again, we'll upload it again.
                        } //Else, the show must go on
                    } //Else, don't care about the file date. (Take in account this is a very rare thing)
                }
            }
            localBlacklist_add(evt.OldPath);
            req1:=NewRequest("remove", evt.OldPath); //Semantically correct
            req1.Destination=0;
            req2:=NewRequest("localBlacklistRemove", evt.OldPath);
            req2.Destination=2;
            req2.Options=O_REQ_DO_ANYWAY;
            reqs=append(reqs, req1, req2);
        }
        case "CREATE","WRITE":{
            var req1, lastReq *Request;
            if evt.Path == "" || evt.FileID == "" {
                return nil, NewPathError("Undefined path or ID for name %q with ID %q", evt.File.Name, evt.FileID);
            }
            if util.IsDir(evt.File.MimeType) { //Only have to create the dir
                req1=NewRequest("ensureDir", evt.Path);
                req1.Destination=0;
            }else { //If the file is blacklisted, it should exist on the local filesystem
                handleReq:=NewRequest("openWrite", evt.Path);
                ioreq<-handleReq;
                err:=<-handleReq.Status;
                if err != nil {
                    return nil,err;
                }
                localBlacklist_add(evt.Path);
                req1=NewRequest("get", evt.FileID);
                req1.File=handleReq.File;
                req1.Destination=1;
                lastReq=NewRequest("localBlacklistRemove",evt.Path);
                lastReq.Destination=2;
                lastReq.Options=O_REQ_DO_ANYWAY;
            }
            req2:=NewRequest("setXattr", evt.Path, evt.FileID);
            req3:=NewRequest("updateTimes", evt.Path, evt.File.ViewedByMeTime, evt.File.ModifiedTime);
            reqs=append(reqs, req1, req2, req3, lastReq); //req1 and lastReq can be nil and will be ignored
        }
        case "RENAME":{
            if evt.Path == "" || evt.OldPath == "" {
                return nil, NewPathError("No path for file with name %q and ID %q", evt.File.Name, evt.FileID);
            }
            newExists:=remoteBlacklist_get(evt.Path);
            oldExists:=remoteBlacklist_get(evt.OldPath);
            if newExists || oldExists {
                return nil,nil;
            }
            req1:=NewRequest("rename", evt.OldPath, evt.Path); //The rename keeps xattrs and times
            req1.Destination=0;
            localBlacklist_add(evt.Path); //Blacklist also the new path where it'll be added
            localBlacklist_add(evt.OldPath);
            req2:=NewRequest("localBlacklistRemove", evt.OldPath);
            req2.Options=O_REQ_DO_ANYWAY;
            req3:=NewRequest("localBlacklistRemove", evt.Path);
            req3.Options=O_REQ_DO_ANYWAY;
            req2.Destination, req3.Destination=2, 2; //Worker thread blacklist handlers
            reqs=append(reqs, req1, req2, req3);
        }
    default:
        return nil, NewUnimplementedType(evt.Type);
    }
    return;
}

//Process a local event and output a bunch (aka slice) of requests and an error
//While messing up with this function, take in account this is the holder of most of sync logic (and not so logic)
//Also, the requestProcessingMutex must be feed manually when the hard job is done
//This function was over-complicated and was completely rewritten
func processLocalEvent(evt *Event) (reqs []*Request, err error){
    if evt == nil || evt.Path == "" && evt.OldPath == "" {
        log.Debugf("Empty local event: %+v\n", evt);
        return nil,nil; //Skip empty events
    }
    filePath:=util.ChooseANonEmptyString(evt.Path, evt.OldPath); //Choose the non-empty one
    //Check blacklist
    if localBlacklist_get(filePath){
        return nil,nil; //File is blacklisted
    }
    log.Debug("Waiting for local event lock");
    requestProcessingMutex.Lock();
    log.Debug("Local event lock acquired");
    remoteFile,err:=getFileFromPaths(evt.Path, evt.OldPath);
    //Get FileID for paths
    if evt.FileID == "" { //A FileID should come for every event (except CREATE and some others before it gets tagged)
        if err == nil{
            evt.FileID=remoteFile.Id;
            if evt.Type == "CREATE" {
                evt.Type = "WRITE"; //Is a WRITE event as it exists on Drive
            }
            setFidRequest:=NewRequest("setXattr", evt.Path, evt.FileID);
            setFidRequest.Destination=0; //localfs
            setFidRequest.Options=O_REQ_MAY_FAIL; //Allow it to fail
            reqs=append(reqs, setFidRequest);
        }else if evt.Type == "REMOVE" {
            requestProcessingMutex.Unlock();
            return nil,nil; //A remove w/o FileID is a remove over a removed file
        }else if evt.Type != "REMOVE"{ //Must be a CREATE event
            //The CREATE event should add a fileID to the conversion tree before freeing the mutex
            evt.FileID, err=getNewFileID(filePath); //Also register it in conversion tree
            if err != nil{
                requestProcessingMutex.Unlock();
                panic(err);
            }
            evt.Type="CREATE"; //If the fileID is not defined, this must be a CREATE event even if the file isn't new to the localfs
        }
    }
    //Create FAMAdd request
    famAddRequest:=NewRequestWithContext(eventsContext, "FAMAdd", evt.FileID, evt.Type);
    famAddRequest.Destination=2; //Worker thread
    eventContext, eventContextCancelFunc:=famAddRequest.Context, famAddRequest.Cancel;
    reqs=append(reqs, famAddRequest);
    //Create wait request for fidCreationProgress map
    var waitRequest *Request;
    if evt.FileID != "" {
        //If the file is being created, wait until it stops
        if _, exists:=fidCreationProgress.Check(evt.FileID); exists{
            waitRequest=NewRequestWithContext(eventContext, "waitFid", evt.FileID);
            waitRequest.Destination=2; //Event worker
        }
    }
    //Check if this is a rename evt
    renamedMapMutex.RLock();
    renamedEvt, wasRenamed:=renamedMap[evt.FileID];
    if wasRenamed {
        log.Debugf("Processing rename on %q(%s)\n", evt.Path, evt.FileID);
        delete(renamedMap, evt.FileID);
    }
    renamedMapMutex.RUnlock();
    if evt.Type != "CREATE" {
        //IMPORTANT: When it comes to the create event, there are still some things to be made before this lock can be released
        requestProcessingMutex.Unlock();
    }
    // ==================================END OF CRITICAL CHAIN============================
    //End of the critical thing. Now we will create the pertinent requests for the gathered data
    log.Debugf("New local event for %+v\n", *evt);
    if waitRequest != nil{
        reqs=append(reqs, waitRequest);
    }
    removeFileFromBlacklist:=NewRequest("remoteBlacklistRemove", evt.Path); //This request is present at the end of every slice
    removeFileFromBlacklist.Destination=2; //Worker thread
    removeFileFromBlacklist.Options=O_REQ_DO_ANYWAY;
    switch evt.Type {
        case "CREATE":{
            mainRequest:=NewRequestWithContext(eventContext, "create", evt.Path, evt.FileID);
            if evt.FileID != "" && mainRequest.Context != nil{
                err=fidCreationProgress.Add(evt.FileID, mainRequest.Context);
            }
            if err != nil {
                return;
            }
            requestProcessingMutex.Unlock();
            //End of critical chain (CREATE BRANCH)
            mainRequest.Destination=1; //Drive
            if wasRenamed { //RENAME LOGIC IS HERE
                mainRequest.Type="move";
                //New path is on the Path field, this filed holds the old parent and the new parent
                mainRequest.Arguments=[]string{renamedEvt.FileID, evt.FileID}; //From, to
            }else{
                mainRequest.File, err=openLocalFile(evt.Path);
            }
            if err != nil {
                return;
            }
            remoteBlacklist_add(evt.Path);
            //This will trigger a local event CHMOD, won't prevent it
            preRequest:=NewRequest("setXattr", evt.Path, evt.FileID);
            preRequest.Destination=0; //Localfs
            preRequest.Options=O_REQ_MAY_FAIL;
            reqs=append(reqs, preRequest, mainRequest, removeFileFromBlacklist);
        }
        case "REMOVE":{
            remoteBlacklist_add(evt.Path);
            mainRequest:=NewRequest("remove", evt.FileID);
            mainRequest.Destination=1; //Drive
            reqs=append(reqs, mainRequest, removeFileFromBlacklist);
        }
        case "WRITE":{
            remoteBlacklist_add(evt.Path);
            mainRequest:=NewRequestWithContext(eventContext, "update", evt.FileID);
            mainRequest.File, err=openLocalFile(evt.Path);
            if err != nil{
                return;
            }
            mainRequest.Destination=1; //Drive
            reqs=append(reqs, mainRequest, removeFileFromBlacklist);
        }
        case "RENAME":{ //For the other part, see the CREATE event
            renamedMapMutex.Lock();
            renamedMap[evt.FileID]=evt;
            renamedMapMutex.Unlock();
            return nil,nil; //Don't return any request
        }
        case "CHMOD":{
            //Stat the local and remote files
            localFile,err:=openLocalFile(evt.Path);
            if err != nil {
                return nil, err;
            }
            localStat,err:=localFile.Stat();
            if remoteFile == nil {
                //Remote file doesn't exist?
                return nil, NewFileNotExistError(evt.Path, evt.FileID);
            }else if err != nil {
                return nil, err;
            }
            //Remote date is already on UTC
            dateR,_:=time.Parse(time.RFC3339, remoteFile.ModifiedTime); //Date for remote file
            dateL:=localStat.ModTime().UTC(); //It's on the local tz
            log.Debugf("Comparing dates:\n\tRemote: %s\n\tLocal: %s\n", dateR, dateL);
            if util.TimeAfterOrEq(dateR, dateL){
                return nil, err;
            }
            remoteBlacklist_add(evt.Path);
            mainRequest:=NewRequest("updateTimestamp", evt.FileID);
            mainRequest.Destination=1; //Drive
            mainRequest.File=localFile;
            reqs=append(reqs, mainRequest, removeFileFromBlacklist);
        }
    }
    for _,req:=range(reqs){
        if req.Context == nil{
            //Add context to all events that doesn't have one
            req.Context, req.Cancel=context.WithCancel(eventContext);
        }
    }
    endFAMRequest:=NewRequest("FAMCancel", evt.FileID);
    endFAMRequest.Destination=2; //Worker thread
    endFAMRequest.Options=O_REQ_DO_ANYWAY; //Should do this also if everything else has failed
    endFAMRequest.Cancel=eventContextCancelFunc;
    reqs=append(reqs, endFAMRequest);
    return;
}

//------- Wrap requests to Drive

//Get a new FileID from the pool and register it for path
func getNewFileID(path string) (id string, err error){
    req:=NewRequest("getFID", path);
    drvreq<-req; //Manual routing
    err=<-req.Status;
    if err == nil {
        id=req.ReturnValue.(string);
    }
    return;
}

//Request a File resource to Drive from a file Path
func getFileFromPath(path string) (*File, error){
    req:=NewRequest("statPath", path);
    drvreq<-req; //Send w/o routing
    err:=<-req.Status;
    if err != nil {
        return nil,err;
    }
    if v,ok:=req.ReturnValue.(*File); ok{ //Type assertion (should return this type)
        return v,nil;
    }else{
        return nil,NewArgumentsError("Expected a File resource but got another thing");
    }
}

//Request a File resource to Drive from various file Paths
func getFileFromPaths(paths ...string) (file *File, err error){
    for _,path:=range(paths) {
        file,err=getFileFromPath(path);
        if err == nil {
            return;
        }
    }
    return;
}

//------- Wrap requests to LocalFS
func openLocalFile(filePath string) (file *os.File, err error){
    req:=NewRequest("open", filePath);
    ioreq<-req;
    err=<-req.Status;
    file=req.File;
    return;
}

//Blacklists logic. This comes from JDrive.
//
//Blacklists are checked from the events watcher goroutine and also managed from workers, so a RW mutex is needed
//
//Local blacklist is called from a remote event handler when working with a local file.
//It's checked by a local event handler when an event is fired from local filesystem.
//
//Remote blacklist is called from a local event handler when working with a remote file.
//It's checked by a remote event handler when an event is fired from the remote filesystem.
//
//This function adds a file to the local blacklist. Path must be not empty.
//Returns a channel to listen for completion.
func localBlacklist_add(path string){
    log.Debugf("Adding %s to local blacklist\n", path);
    /* #nosec */
    localBlacklist.Add(path);
}

//Get a channel or nil if the file is on local blacklist or not
func localBlacklist_get(path string) bool{
    return localBlacklist.Get(path);
}

//Remove a file from the local blacklist
func localBlacklist_remove(path string){
    log.Debugf("Removing %s from local blacklist\n", path);
    /* #nosec */
    localBlacklist.Remove(path);
}

//Add a file to the remote blacklist. Acts like the local counterpart
func remoteBlacklist_add(path string){
    log.Debugf("Adding %s to remote blacklist\n", path);
    /* #nosec */
    remoteBlacklist.Add(path);
}

//Get a channel or nil from remote blacklist. Acts like its local counterpart
func remoteBlacklist_get(path string) bool{
    return remoteBlacklist.Get(path);
}

func remoteBlacklist_remove(path string){
    log.Debugf("Removing %s from remote blacklist\n", path);
    /* #nosec */
    remoteBlacklist.Remove(path);
}

func workerFarm(ctx context.Context, num int){
    //Usually 1 worker is stable, but transfers will be very slow (1 event at a time). 2 Workers are enough for a normal use, but heavy use needs 4 or more workers.
    //Max worker number is 8 (not really a max as you can specify more, but a warning is thrown) to prevent ratelimiting.
    nworkers:=2; //Default to muti-threading
    if num > 0 {
        nworkers=num;
    } else if num < 0 {
        panic("Bad workers number. Must be > 0");
    }
    if nworkers > 8 {
        log.Warnf("Using %d workers. This can lead to bugs due to ratelimiting. Be sure your Drive credentials support this\n", nworkers);
    } else {
        log.Infof("Starting worker farm with %d workers\n", nworkers);
    }

    wg.Add(nworkers);
    for i:=0; i < nworkers; i++ {
        go workerThread(ctx, &requestSlice, requestSliceMutex);
    }
}

//This function keeps running and sending Requests from requests map until stop is fired
func workerThread(ctx context.Context, reqs *[][]*Request, mutex *sync.Mutex){
    defer wg.Done();
    var (
        exit bool;
        req []*Request;
    )
    for !exit {
        mutex.Lock();
        if len(*reqs) > 0 {
            req,*reqs=(*reqs)[0], (*reqs)[1:];
        }
        mutex.Unlock();
        if req != nil {
            var failedAtSomePoint bool;
            for i,r:=range(req){
                if r==nil{ //Those if statements are separated because are treating different situations
                    continue;
                }
                if failedAtSomePoint && r.Options&O_REQ_DO_ANYWAY == 0 { //Ignore request
                    log.Debugf("Prior event failed or was canceled and O_REQ_DO_ANYWAY is not set on request %s %q\n", r.Type, r.Path);
                    continue;
                }
                err:=processRequest(r);
                if err != nil { //An error on a single request causes the entire slice to be rejected
                    var allowedStr string;
                    if r.Options&O_REQ_MAY_FAIL == O_REQ_MAY_FAIL {
                        allowedStr="(allowed to fail)";
                    }
                    log.Errorf("Error handling request%s %d\n\tRequest: %+v\n\tError: %s\n", allowedStr, i, r, err.Error());
                    failedAtSomePoint=true;
                }else if e:=ctx.Err(); e != nil{
                    log.Warnf("Worker interrupted by context: %s\n", e);
                    break; //Exit loop imediately
                }
            }
        }else{
            time.Sleep(1*time.Second); //If no events in queue, sleep. Don't sleep when there are events to be processed
        }
        req=nil; //Set to nil before we continue
        select{
        case <-ctx.Done():
            exit=true;
        default:
        }
    }
}

//This function processes requests and sends them to the correct channel. Also it waits for it to terminate and catches the errors.
//The function should be called only from a worker thread as it will block until Request is processed.
func processRequest(req *Request) error{
    if req == nil { //Ignore nil requests
        return nil;
    }
    switch req.Destination {
    case 0:
        ioreq<-req;
    case 1:
        drvreq<-req;
    case 2: //Actions that should be done by worker
        err:=processWorkerReq(req);
        req.Status<-err;
        return err;
    default:
        log.Errorf("Error on request processing. Unknown destination %d\n", req.Destination);
    }
    //This channel won't return nothing
    var actualContext context.Context;
    if req.Cancel != nil && req.Context != nil{
        defer req.Cancel();
        actualContext=req.Context;
    }else{
        actualContext=eventsContext;
    }
    var st error;
    select{
    case st=<-req.Status:
    case <-actualContext.Done():
        if ctxError:=actualContext.Err(); ctxError!=nil{
            return ctxError;
        }
        return nil; //Exit now!
    }
    if st != nil {
        log.Error(st.Error());
    }
    return st;
}

func processWorkerReq(req *Request) (err error){
    if req.Path == "" {
        return NewArgumentsError("Path was empty when processing worker request");
    }
    switch req.Type{
    case "localBlacklistRemove":
        localBlacklist_remove(req.Path);
    case "remoteBlacklistRemove":
        remoteBlacklist_remove(req.Path);
    case "setXattrFromCTree": //This event is here because this is the only package that can access both drive and localfs
        err=setXattrFromCTree(req.Path);
    case "waitFid":
        _,err=fidCreationProgress.Wait(eventsContext, req.Path);
    case "FAMAdd":
        log.Debugf("Registering %q for %v on FAM\n", req.Path, req.Arguments);
        if len(req.Arguments) < 1{
            err=NewArgumentsError("expected to have 1 argument on FAMAdd");
        }else{
            err=fileActionsMap.Register(req.Path, req.Arguments[0], req.Context, req.Cancel);
        }
    case "FAMCancel":
        log.Debugf("Canceling %q\n", req.Path);
        if req.Cancel==nil{
            err=NewArgumentsError("CancelFunc can't be nil on call to FAMCancel");
        }
        req.Cancel();
    default:
        err=NewUnimplementedType(req.Type);
    }
    return;
}

func setXattrFromCTree(pth string) error{
    f,err:=getFileFromPath(pth);
    if err != nil {
        return err;
    }
    req:=NewRequest("setXattr", pth, f.Id);
    ioreq<-req; //Send w/o routing. I'm the router *visible confusion*
    err=<-req.Status;
    return err; //Maybe nil, maybe an error
}
