module gitlab.com/mrvik/gogdrive

go 1.12

require (
	cloud.google.com/go v0.54.0 // indirect
	github.com/akamensky/argparse v1.2.1
	github.com/fsnotify/fsnotify v1.4.7
	github.com/pkg/xattr v0.4.1
	gitlab.com/mrvik/anymatch v0.0.0-20190806160905-ecef2d66eb32
	gitlab.com/mrvik/logger v0.0.0-20200120150936-91f08edcaa9b
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527
	google.golang.org/api v0.20.0
	google.golang.org/genproto v0.0.0-20200306153348-d950eab6f860 // indirect
)
