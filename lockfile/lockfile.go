//This package defines a State structure with some values saved by gogdrive.
package lockfile;

import(
    "fmt"
    "os"
    "io/ioutil"
    "encoding/json"
    "sync"
  . "gitlab.com/mrvik/gogdrive/types"
)

//Gogdrive status representation. Got from lockfile and written to it
//Functions using state should be aware of empty fields when state file is not present
type State struct{
    sync.RWMutex;
    //Status holds the status of gogdrive at the moment of stopping
    //StartPageToken holds the last page token processed (changes transformed to events and sent via channel)
    status, StartPageToken string;
    //Path where lockfile should be stored. It isn't exported so JSON.Marshal shouldn't mess it up
    path string;
}

//Initialize a new state instance. Error holds a possible error while reading or unmarshaling
func NewState(config *Config) (*State, error){
    rt:=&State{
        path: *config.Files.Lockfile,
    };
    err:=rt.Read();
    return rt,err;
}

//Read lockfile and store it
func (st *State) Read() error{
    st.Lock();
    defer st.Unlock();
    content,err:=ioutil.ReadFile(st.path);
    if err != nil{
        if os.IsNotExist(err) { //Leave state empty
            return nil;
        }else{
            return err;
        }
    }
    err=json.Unmarshal(content, st);
    return err;
}

//Update the lockfile with a new start page token
func (st *State) UpdateSPToken(token string) error{
    if st == nil {
        return fmt.Errorf("can't update a nil state: %+v", st);
    }
    st.Lock();
    st.StartPageToken=token;
    st.Unlock();
    return st.Write();
}

func (st *State) Write() error{
    st.RLock();
    defer st.RUnlock();
    content,err:=json.Marshal(st);
    if err != nil {
        return err;
    }
    return ioutil.WriteFile(st.path, content, 0600);
}

func (st *State) ChangeStatus(status string){
    st.Lock();
    defer st.Unlock();
    st.status=status;
    go st.Write();
}
