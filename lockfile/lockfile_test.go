package lockfile

import (
  . "gitlab.com/mrvik/gogdrive/types"
    "os"
    "path"
    "testing"
)

const (
    FAKE_TOKEN="83752734652439572";
);
var (
    LOCKFILE_PATH=path.Join(os.TempDir(), "gogtest.lock");
    config=&Config{
        Files: ConfigFiles{
            Lockfile: &LOCKFILE_PATH,
        },
    };
);

func TestState(t *testing.T){
    state,err:=NewState(config);
    if state == nil {
        if err != nil {
            t.Fatal(err.Error());
        }else{
            t.Fatal("State was nil");
        }
    }
    err=state.UpdateSPToken(FAKE_TOKEN); //Of course it's not real
    if err != nil {
        t.Error(err.Error());
    }else{
        t.Log("Token updated OK and file written");
    }
    t.Run("TestRead", afterCheck);
    if err:=os.Remove(LOCKFILE_PATH); err!=nil{
        t.Error(err.Error());
    }
}

func afterCheck(t *testing.T){
    state,err:=NewState(config);
    if err != nil {
        t.Fatal(err.Error());
    }
    if state.StartPageToken != FAKE_TOKEN {
        t.Errorf("Bad token while testing lockfile. Got %q but expected %q\n", state.StartPageToken, FAKE_TOKEN);
    }else{
        t.Logf("Got %q as lockfile token. OK\n", state.StartPageToken);
    }
}
