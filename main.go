/*
   This package orchestrates the whole software.
   For instructions on how to install it, refer to the README.
   For reference of CLI parameters, see gogdrive(1)
 */
package main;

import (
    "context"
    "gitlab.com/mrvik/gogdrive/auth"
    "gitlab.com/mrvik/gogdrive/drive"
    "gitlab.com/mrvik/gogdrive/localfs"
    "gitlab.com/mrvik/gogdrive/signals"
    // "gitlab.com/mrvik/gogdrive/lockfile"
    // "gitlab.com/mrvik/gogdrive/diff"
    "gitlab.com/mrvik/gogdrive/events"
    "gitlab.com/mrvik/gogdrive/config"
    "gitlab.com/mrvik/gogdrive/types"
    logger "gitlab.com/mrvik/logger/log"
    . "gitlab.com/mrvik/logger"
    "os"
    "sync"
);

const(
    //Max events without processing on event buffer. The more events here, the more events are lost if program closes
    //This souldn't be a problem as long as event processing goroutines won't be too busy
    MAX_EVENT_BUFFER=2;
)

var (
    //Is it being debugged? Set on compile time
    DEBUG string;
    log Logger;
    wg sync.WaitGroup;
)

func main(){
    //Create context
    ctx, stopFn:=context.WithCancel(context.Background());
    if DEBUG == "" {
        log=logger.Create("", nil, nil);
        defer recoverAndExit(stopFn);
    }else{
        log=logger.CreateDebug("", nil, nil);
        log.Debug("Debug enabled. ---> THIS BUILD IS NOT SUITABLE FOR PRODUCTION <---");
    }
    //Channels
    ioreq:=make(chan *types.Request, MAX_EVENT_BUFFER); //IO Request channel
    drvreq:=make(chan *types.Request, MAX_EVENT_BUFFER); //Drive Request channel
    localev:=make(chan *types.Event, MAX_EVENT_BUFFER); //Local event channel
    remoteev:=make(chan *types.Event, MAX_EVENT_BUFFER); //Remote event channel
    //Start signal catching (on a separate goroutine)
    wg.Add(1);
    go signals.Init(ctx, stopFn, &wg, log);
    //Init config
    config.Init(log);
    log.Debugf("\nCurrent config:\n%+v\n", config.Config);
    cnfp:=&config.Config; //Create pointer to config instead of being always taking address
    client:=auth.Init(ctx, log, cnfp); //Get OAuth2 http client.
    err:=localfs.Init(ctx, log, cnfp, &wg, ioreq, localev);
    panicErr(err);
    err=drive.Init(ctx, log, cnfp, client, &wg, drvreq, remoteev);
    panicErr(err);
    events.Init(ctx, log, cnfp, &wg, ioreq, drvreq, localev, remoteev);
    panicErr(err);
    log.Debug("Goroutines initialized. Now waiting for them to terminate");
    <-ctx.Done();
    log.Debug("Context cancelled, waiting for goroutines...");
    wg.Wait(); //Wait for goroutines to terminate
    log.Info("Exiting");
}

func panicErr(e error){
    if e != nil {
        panic(e);
    }
}

func recoverAndExit(cancel context.CancelFunc){
    if e:=recover(); e != nil {
        cancel();
        log.Errorf("Error while starting gogdrive: %s\n", e);
        os.Exit(1);
    }
}
