package config;

import (
    "os"
    "path"
    "testing"
)

func TestGetPath(t *testing.T){
    home,err:=os.UserHomeDir();
    if err != nil {
        t.Error(err.Error());
    }
    tests:=[]struct{args [3]string; expected string}{
        {
            [3]string{home, "", "testing1"},
            path.Join(home, "testing1"),
        },
        {
            [3]string{home, "test", "testing1"},
            path.Join(home, "test"),
        },
        {
            [3]string{home, "/something", "nothing"},
            "/something",
        },
        {
            [3]string{home, "", "/this/is/the/selected/one"}, //Default should be over the base
            path.Join(home, "/this/is/the/selected/one"),
        },
    }
    for _,test:=range(tests){
        res:=*getPath(test.args[0], &test.args[1], test.args[2]);
        if res != test.expected {
            t.Errorf("Expected %q for %v but got %q\n", test.expected, test.args, res);
        }else{
            t.Logf("Got %q for %v as expected\n", res, test.args);
        }
    }
}
