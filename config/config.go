package config;

import (
    "encoding/json"
    "fmt"
    "gitlab.com/mrvik/gogdrive/types"
    "gitlab.com/mrvik/logger"
    "github.com/akamensky/argparse"
    "io/ioutil"
    "os"
    "path"
    "path/filepath"
);

const (
    NAME = "gogdrive";
    DESCRIPTION = "Sync your Google Drive files without a GUI"
);

var (
    log logger.Logger;
    Config types.Config;
    defaultBase=path.Join(os.Getenv("HOME"), ".config/gogdrive");
);

func Init(lg logger.Logger){
    log=lg;
    fc:=parseArguments();
    Config=types.NewConfig(&fc);
    Config.Files.UserConfig=getPath(*fc.Base, fc.Files.UserConfig, "gogdrive.json"); //Fix file path before reading it
    err:=readConfig("/etc/gogdrive.json");
    if err != nil{
        log.Warnf("No system-wide config: %s\n", err);
    }
    err=readConfig(*Config.Files.UserConfig);
    if err != nil{
        log.Warnf("No user config: %s\n", err);
    }
    fixPaths(fc);
    if Config.Local.Modes.Dir != 0 {
        err=os.MkdirAll(*Config.Base, Config.Local.Modes.Dir);
    } else {
        err=os.MkdirAll(*Config.Base, 0700);
    }
    if err != nil{
        panic(err);
    }
}

func parseArguments() types.Config{
    parser:=argparse.NewParser(NAME, DESCRIPTION);
    defer recoverArgumentsError();
    fconfig:=types.Config{
        Base: parser.String("b", "config-dir", &argparse.Options{Help: "Base directory for config", Default: defaultBase}),
        Files: types.ConfigFiles{
            UserConfig: parser.String("", "config", &argparse.Options{Help: "Base directory for config"}),
            Credentials: parser.String("c", "credentials", &argparse.Options{Help: "Custom credentials file (relative to base)"}),
            Token: parser.String("t", "token", &argparse.Options{Help: "Your access token file (relative to base)"}),
            Lockfile: parser.String("l", "lockfile", &argparse.Options{Help: "Gogdrive lockfile (relative to base)"}),
            Ignore: parser.String("i", "ignore-file", &argparse.Options{Help: "gitignore-like file (relative to base)"}),
            LocalDir: parser.String("d", "local-dir", &argparse.Options{Help: "Absolute path to sync your Google Drive"}),
        },
        Drive: types.DriveConfig{
            RemoveOrphans: parser.Flag("", "remove-orphans", &argparse.Options{Help: "Remove orphaned files from Drive (following trash configuration)"}),
        },
        AuthOnly: parser.Flag("", "auth-only", &argparse.Options{Help: "Auth and exit"}),
        DryRun: parser.Flag("", "dry-run", &argparse.Options{Help: "Auth, log changes and exit"}),
        ID: parser.String("", "id", &argparse.Options{Help: "Gogdrive instance ID"}),
    };
    err:=parser.Parse(os.Args);
    if err != nil {
        panic(err);
    }
    return fconfig;
}

func fixPaths(cnf types.Config){
    if cnf.Base == nil || *cnf.Base == "" {
        base:=generateBasePath(&cnf);
        cnf.Base=&base;
        Config.Base=&base;
    }
    Config.Files.UserConfig=getPath(*cnf.Base, cnf.Files.UserConfig, "gogdrive.json");
    Config.Files.Credentials=getPath(*cnf.Base, cnf.Files.Credentials, "credentials.json");
    Config.Files.Token=getPath(*cnf.Base, cnf.Files.Token, "token.json");
    Config.Files.Lockfile=getPath(*cnf.Base, cnf.Files.Lockfile, "lockfile.json");
    Config.Files.Ignore=getPath(*cnf.Base, cnf.Files.Ignore, ".syncignore");
    if cnf.Files.LocalDir == nil || *cnf.Files.LocalDir == "" {
        var p string;
        if cnf.ID != nil && *cnf.ID != "" {
            p=path.Join(os.Getenv("HOME"), fmt.Sprintf("GoogleDrive-%s", *cnf.ID));
        }else{
            p=path.Join(os.Getenv("HOME"), "Google Drive");
        }
        Config.Files.LocalDir=&p;
    }else{
        Config.Files.LocalDir=cnf.Files.LocalDir; //Not checking if the path is absolute. Leaving you alone shooting your foot
    }
}

//Generate base path for config files taking in account the runtime ID
func generateBasePath(cnf *types.Config) string{
    if id:=*cnf.ID; id == "" {
        return defaultBase;
    }else{
        return fmt.Sprintf("%s-%s", defaultBase, id);
    }
}

//Read config. Modifies the Config struct directly.
//Returns an error or nil
func readConfig(file string) error{
    content,err:=ioutil.ReadFile(filepath.Clean(file));
    if err != nil {
        return err;
    }
    err=json.Unmarshal(content, &Config);
    if err != nil {
        panic(err);
    }
    return nil;
}

//getPath will return a string pointer result of base+default if value is nil, value if it contains an absolute path or base+value if value is a relative path
func getPath(base string, vnow *string, def string) *string{
    var rt string;
    if vnow==nil || *vnow=="" { //Not defined
        rt=path.Join(base, def);
    }else if path.IsAbs(*vnow) { //Defined and absolute
        rt=*vnow;
    }else{ //Defined and relative
        rt=path.Join(base, *vnow);
    }
    return &rt;
}

func recoverArgumentsError(){
    if e:=recover(); e!=nil{
        log.Errorf("Error parsing arguments: %s\n", e);
        os.Exit(1);
    }
}
