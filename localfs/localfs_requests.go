package localfs;

import(
    "context"
    "github.com/pkg/xattr"
  . "gitlab.com/mrvik/gogdrive/types"
    "os"
    "path"
    "path/filepath"
    "time"
)

//Listen to requests via channel. See Request type for more info
//Take a look at the cases. Every case calls a function on a separate goroutine.
//This is important because one thread can only process one request, so calling different goroutines may speed up this process.
func fsRequestsListener(ctx context.Context){
    defer wg.Done();
    var exit bool;
    for !exit {
        select {
        case <-ctx.Done():
            exit=true;
        case req,ok:=<-ioreq:
            if !ok {
                panic("IO Thread couldn't read from channel. It's been closed");
            }
            switch req.Type{
            case "open":
                go openFile(req);
            case "openWrite":
                go openFileWrite(req);
            case "ensureDir":
                go ensureDir(req);
            case "stat":
                go stat(req);
            case "updateTimes":
                go updateFileTimes(req);
            case "setXattr":
                go setXattr(req);
            case "getXattr":
                go getXattr(req);
            case "rename":
                go renameFile(req);
            case "remove":
                go removeFile(req);
            case "registerBlacklist":
                registerBlacklist(req); //Run in the same goroutine
            default:
                req.Status<-NewUnimplementedType(req.Type);
            }
        }
    }
}

//IO Functions
//Generic open file. Return io.Reader for a file from arguments
func gOpenFile(relpath string) (*os.File,error){
    pth:=path.Join(localdir, relpath);
    return os.Open(filepath.Clean(pth));
}

func openFile(req *Request){
    wg.Add(1);
    defer wg.Done();
    rt,err:=gOpenFile(req.Path);
    req.File=rt;
    req.Status<-err;
}

//Open a file and return a writer. Create it and needed paths if doesn't exist
func openFileWrite(req *Request){
    wg.Add(1);
    defer wg.Done();
    pth:=path.Join(localdir, req.Path);
    err:=os.MkdirAll(path.Dir(pth), dmode);
    if err != nil {
        req.Status<-err;
        return;
    }
    handle,err:=os.OpenFile(pth, os.O_RDWR|os.O_CREATE|os.O_TRUNC, fmode);
    req.File=handle;
    req.Status<-err;
}

//Ensure a dir exists. If it doesn't, create it
func ensureDir(req *Request){
    wg.Add(1);
    defer wg.Done();
    pth:=path.Join(localdir, req.Path);
    info,err:=os.Stat(pth);
    if err != nil && !os.IsNotExist(err) { //Error was fired and is not related to the nonexistence of the dir
        req.Status<-err;
    } else if err != nil { //Doesn't exist
        err=os.MkdirAll(pth, dmode);
        req.Status<-err;
    }else if info.IsDir() { //Already exists and it's a directory
        req.Status<-nil;
    }else{ //Already exists and it isn't a directory. Status will be error
        req.Status<-NewIsFileError(req.Path);
    }
}

//Get stat info from file
func stat(req *Request){
    wg.Add(1);
    defer wg.Done();
    pth:=path.Join(localdir, req.Path);
    var info os.FileInfo; //Declare variable type because there's no type checking on ReturnValue interface{}
    info,err:=os.Stat(pth); //Error check is done on the request side
    req.ReturnValue=&info;
    req.Status<-err;
}

func setXattr(req *Request){
    //Don't try to set xattrs if aren't supported
    if !enableXattrs{
        req.Status<-nil;
        return;
    }
    wg.Add(1);
    defer wg.Done();
    pth:=path.Join(localdir, req.Path);
    name:=XATTR_PREFIX;
    var content string;
    switch len(req.Arguments) {
    case 1:
        content=req.Arguments[0];
    case 2:
        name=req.Arguments[0];
        content=req.Arguments[1];
    default:
        req.Status<-NewArgumentsError("Bad number of arguments");
        return;
    }
    req.Status<-xattr.Set(pth, name, []byte(content));
}

func getXattr(req *Request){
    //Return nil if xattrs aren't supported
    if !enableXattrs{
        req.ReturnValue="";
        req.Status<-nil;
        return;
    }
    wg.Add(1);
    defer wg.Done();
    pth:=path.Join(localdir, req.Path);
    name:=XATTR_PREFIX;
    if len(req.Arguments) >= 1 {
        name=req.Arguments[0];
    }
    val,err:=xattr.Get(pth, name);
    rt:=string(val);
    if err == xattr.ENOATTR && rt == "" && name == XATTR_PREFIX { //Test compat xattr before giving up
        log.Debugf("XAttr not found for %s. Trying with compat prefix\n", pth);
        req.Arguments=[]string{COMPAT_PREFIX};
        getXattr(req);
        return;
    }
    req.ReturnValue=string(val);
    req.Status<-err;
}

func removeFile(req *Request){
    wg.Add(1);
    defer wg.Done();
    if req.Path == "" {
        req.Status<-NewArgumentsError("Path expected to be a non-empty string");
        return;
    }
    pth:=path.Join(localdir, req.Path);
    req.Status<-os.RemoveAll(pth);
}

//Rename a file from Path on request to new path on argument 0
func renameFile(req *Request){
    wg.Add(1);
    defer wg.Done();
    if ln:=len(req.Arguments); ln < 1 {
        req.Status<-NewArgumentsError("Expected 1 argument to rename file");
        return;
    }
    pth1:=path.Join(localdir, req.Path);
    pth2:=path.Join(localdir, req.Arguments[0]);
    if pth1 == "" || pth2 == "" {
        req.Status<-NewArgumentsError("Got an empty path to rename");
        return;
    }
    st:=os.Rename(pth1, pth2);
    req.Status<-st;
}

//Update atime and mtime for the file specified by Path. atime and mtime can be empty. If they're, will be left untouched
func updateFileTimes(req *Request){
    wg.Add(1);
    defer wg.Done();
    if req.Path == "" {
        req.Status<-NewArgumentsError("Invalid path to updateFileTimes");
        return;
    }
    pth:=path.Join(localdir, req.Path);
    var atime, mtime time.Time;
    var err error;
    ln:=len(req.Arguments);
    if ln >= 1 &&req.Arguments[0] != "" {
        atime,err=time.Parse(time.RFC3339, req.Arguments[0]);
        if err != nil {
            req.Status<-err;
            return;
        }
    }
    if ln >= 2 &&req.Arguments[1] != "" {
        mtime,err=time.Parse(time.RFC3339, req.Arguments[1]);
        if err != nil {
            req.Status<-err;
            return;
        }
    }
    req.Status<-os.Chtimes(pth, atime, mtime);
}

//Set the localBlacklist to the sent pointer
func registerBlacklist(req *Request){
    var err error;
    if v,ok:=req.ReturnValue.(*MutexedBlacklist); ok && v != nil{
        localBlacklist=v;
        blacklistReady.Done();
        log.Debug("Blacklist registered on localfs");
    }else{
        err=NewArgumentsError("Expected *MutexedBlacklist and got %T", req.ReturnValue);
    }
    req.Status<-err;
}
