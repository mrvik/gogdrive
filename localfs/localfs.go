//This package provides functions allowing to control the local filesystem.
//Also provides functions scoped on the sync dir. This functions are available as a service (made of type.Request)
package localfs;

import (
    "context"
    "fmt"
    "gitlab.com/mrvik/anymatch"
  . "gitlab.com/mrvik/logger"
    "github.com/fsnotify/fsnotify"
  . "gitlab.com/mrvik/gogdrive/types"
    "github.com/pkg/xattr"
    "os"
    "path"
    "path/filepath"
    "sync"
    "time"
);

const (
    //This is the XATTR name used by gogdrive to mark the files with their fileID.
    XATTR_PREFIX="user.gogdrive-fileID";
    COMPAT_PREFIX="user.jdrive-fileId"; //Compat with JDrive
    EVENT_TIMEOUT=300;
)

var (
    localdir string;
    config *Config;
    log Logger;
    dmode, fmode os.FileMode;
    wg *sync.WaitGroup;
    ioreq chan *Request;
    enableXattrs bool;
    blacklistReady=new(sync.WaitGroup);
    localBlacklist *MutexedBlacklist;
    eventPool *EventWaitPool;
)

//This function initializes all the services that localfs gives. Listens for requests on iorqch and emits events via eventChannel
func Init(ctx context.Context, lg Logger, cnfp *Config, wtg *sync.WaitGroup, iorqch chan *Request, eventChannel chan<- *Event) error{
    blacklistReady.Add(1);
    config=cnfp;
    log=lg;
    localdir=*config.Files.LocalDir;
    wg=wtg;
    ioreq=iorqch;
    if config.Local.Modes.Dir == 0 {
        dmode=0700;
    } else {
        dmode=config.Local.Modes.Dir;
    }
    if config.Local.Modes.File == 0 {
        fmode=0600;
    } else {
        fmode=config.Local.Modes.File;
    }
    err:=anymatch.InitFile(*config.Files.Ignore);
    if err != nil {
        log.Warnf("Can't find a sync ignore file on %s\n", *config.Files.Ignore);
    }
    enableXattrs=testXattrs();
    //Start local filesystem services
    wg.Add(1);
    go fsRequestsListener(ctx);
    err=initDir();
    if err != nil {
        return err;
    }
    //Start filesystem watcher
    err=startWatcher(ctx);
    if err != nil {
        return err;
    }
    //Create the event pool
    eventPool=NewEventWaitPool(eventChannel, EVENT_TIMEOUT);
    //Start the tick watcher
    wg.Add(1);
    go startTickWatcher(ctx);
    //Goroutines are ready. Now return to main
    return nil;
}

func startTickWatcher(ctx context.Context){
    defer wg.Done();
    var exit bool;
    ticker:=time.NewTicker(time.Millisecond*(EVENT_TIMEOUT/2));
    for !exit{
        select{
        case <-ctx.Done():
            ticker.Stop();
            exit=true;
        case <-ticker.C:
            eventList.Iterate(createAndSend);
        }
    }
}

//This function has the same signature as the Iterate function takes so we can pass this directly
//The function takes a event or an error, otherwise it does panic
func createAndSend(event interface{}){
    switch value:=event.(type){
    case error:
        log.Errorf("Error from localfs: %s\n", value);
    case fsnotify.Event:
        evs:=generateEvents(value);
        for _,ev:=range(evs) {
            eventPool.Add(ev.Path, ev);
        }
    default:
        panic(fmt.Errorf("unrecognized type from events: %T", event));
    }
}

//This function initializes the local sync directory in case it doesn't exist
func initDir() error{
    //After having initialized everything, now it's time to ensure the sync directory exists
    rq:=NewRequest("ensureDir", ""); //The localdir itself
    ioreq<-rq;
    return <-rq.Status;
}

//This function tests for xattr support on filesystem containing the sync folder. Returns a value depending on it.
func testXattrs() bool{
    testfile:=path.Join(localdir, "test-gogdrive-xattrs");
    file,err:=os.OpenFile(testfile, os.O_RDWR|os.O_CREATE, 0600);
    if err != nil {
        return false;
    }
    defer os.Remove(testfile);
    defer file.Close();
    err=xattr.FSet(file, XATTR_PREFIX, []byte("sometestvalue"));
    return err == nil;
}

//This function generates a slice of types.Event from a fsnotify.Event.
func generateEvents(original fsnotify.Event) []*Event{
    nm:=goodPath(original.Name);
    //At this point, the local blacklist is not nil
    if localBlacklist.Get(nm) {
        return nil;
    }
    rq:=NewRequest("getXattr", nm);
    ioreq<-rq;
    events:=getEvents(original.Op);
    vts:=make([]*Event,len(events));
    evt:=Event{
        Path: nm,
        FullPath: original.Name,
        Origin: 0, //Got from local filesystem
        Propagate: true, //True by default
        Time: time.Now(),
    }
    err:=<-rq.Status;
    if err == nil {
        if v,ok:=rq.ReturnValue.(string); ok {
            evt.FileID=v;
        }else{
            log.Warnf("Incorrect FileID type %T\n", rq.ReturnValue);
        }
    }
    for i,v:=range(events) {
        cp:=evt;
        cp.Type=v;
        vts[i]=&cp;
        go useTheEventIfYouWant(cp); //Use the event on a parallel goroutine but copy it instead of passing a pointer to avoid race conditions
    }
    return vts;
}

//Get the path relative to sync folder. All functions here operate with relative paths.
//Don't be afrait of the first slash, it still is relative, but Drive paths are generated with the slash and this way it's better (as it's an absolute path on Drive).
func goodPath(fullpath string) string{
    p,err:=filepath.Rel(localdir, fullpath);
    if err != nil {
        panic(err);
    }
    return "/"+p;
}

//Consume an event to do localfs tasks like adding new folders to watch
//Note that we don't need to remove deleted files/directories from watch becouse fsnotify does itself
func useTheEventIfYouWant(evt Event){
    switch evt.Type {
    case "CREATE", "RENAME":
        stat,err:=os.Stat(evt.FullPath);
        if err != nil {
            if !os.IsNotExist(err) {
                log.Warn(err.Error());
            }
            return;
        }
        dir:=stat.IsDir();
        if dir {
            err=filepath.Walk(evt.FullPath, addFromWalk);
            if err != nil{
                log.Warn(err.Error());
            }
        }
    }
}

//Compare fsnotify.Op to get the real events.
//Copied from fsnotify.Op.String and modified to return a slice of strings
func getEvents(op fsnotify.Op)[]string{
    rt:=[]string{};
    if op&fsnotify.Create == fsnotify.Create {
        rt=append(rt, "CREATE");
    }
    if op&fsnotify.Remove == fsnotify.Remove {
        rt=append(rt, "REMOVE");
    }
    if op&fsnotify.Write == fsnotify.Write {
        rt=append(rt, "WRITE");
    }
    if op&fsnotify.Rename == fsnotify.Rename {
        rt=append(rt, "RENAME");
    }
    if op&fsnotify.Chmod == fsnotify.Chmod {
        rt=append(rt, "CHMOD");
    }
    return rt;
}

