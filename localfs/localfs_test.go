package localfs

import (
    "context"
  . "gitlab.com/mrvik/gogdrive/types"
    "io/ioutil"
    "os"
    "path"
    "reflect"
    "sync"
    "time"
    "testing"
)

const (
    TEST_FILE_PATH="file1";
    TEST_FILE_PATH2="dir1/file2";
    TEST_DIR_PATH="dir1/otherdir";
    TEST_XATTR_CONTENT="some content for xattr";
)

var (
    TEST_FILE_TIME=time.Unix(2000, 0).Format(time.RFC3339);
    TEST_FILE_CONTENT=[]byte("some content");
)

type rqtest struct{
    name string;
    fn func(*testing.T);
}

//Test Request handlers
func TestRequestHandlers(t *testing.T){
    //Start it and send a simple one
    wg=new(sync.WaitGroup);
    localdir=path.Join(os.TempDir(), "gogdrive-test"); //Use the temp filesystem
    dmode=0700;
    fmode=0600;
    req:=NewRequest("ensureDir", "/"); //Should exist, but won't be checking it. Only a valid response
    req2:=NewRequest("thisOneDoesn'tExist", "");
    ctx, stop:=context.WithCancel(context.Background());
    ioreq=make(chan *Request, 1);
    wg.Add(1);
    go fsRequestsListener(ctx);
    ioreq<-req;
    ioreq<-req2;
    if err:=<-req.Status; err != nil {
        //Check return type
        t.Fatal(err.Error());
    }
    if err:=<-req2.Status; err == nil {
        t.Fatal("A nonexistent call didn't return an error");
    }
    tests:=[]rqtest{
        {"openWrite", testOpenWrite}, //Create file to test
        {"open", testOpen}, //Open and read that file
        {"ensureDir", testEnsureDir}, //Ensure a dir exists
        {"updateTimes", testUpdateTimes}, //Update mtime
        {"stat", testStat}, //Stat the created dir and file
    };
    xattrTests:=[]rqtest{
        {"setXattr", testSetXattr}, //Set a xattr on the selected file
        {"getXattr", testGetXattr}, //Get the xattr on the same one
    };
    final:=[]rqtest{
        {"rename", testRename}, //Rename file
        {"remove", testRemove}, //Remove file on the new location
    }
    enableXattrs=testXattrs();
    if enableXattrs {
        tests=append(tests, xattrTests...);
    }
    tests=append(tests, final...);
    for _,test:=range(tests){
        t.Logf("Running test for %q\n", test.name);
        if sc:=t.Run(test.name, test.fn); sc {
            t.Logf("Test %s succedded\n", test.name);
        }
    }
    stop();
    t.Log("Waiting for goroutine to terminate");
    waitCtx,waitCancel:=context.WithTimeout(context.Background(), time.Second*5);
    defer waitCancel();
    select{
    case <-waitForGroup(wg):
    case <-waitCtx.Done():
        t.Error("Wait timed out after 5 seconds");
    }
}

//Wait for a WG
func waitForGroup(w *sync.WaitGroup) chan struct{}{
    ch:=make(chan struct{});
    go func(){
        wg.Wait();
        close(ch);
    }();
    return ch;
}

//Test openWrite call creating a new file
func testOpenWrite(t *testing.T){
    rq:=NewRequest("openWrite", TEST_FILE_PATH);
    ioreq<-rq;
    if err:=<-rq.Status; err != nil {
        t.Error(err.Error());
        return;
    }
    defer rq.File.Close();
    count,err:=rq.File.Write(TEST_FILE_CONTENT);
    if err != nil {
        t.Error(err.Error());
    }else{
        t.Logf("OpenWrite test OK. Written %d bytes to %q\n", count, TEST_FILE_PATH);
    }
}

//Test read file comparing to previously written one
func testOpen(t *testing.T){
    req:=NewRequest("open", TEST_FILE_PATH);
    ioreq<-req;
    if err:=<-req.Status; err != nil {
        t.Error(err.Error());
        return;
    }
    defer req.File.Close();
    if content,err:=ioutil.ReadAll(req.File); err != nil {
        t.Error(err.Error());
    }else if !reflect.DeepEqual(content, TEST_FILE_CONTENT) {
        t.Errorf("%v not equals to previously written %v. Len %d and %d\n", content, TEST_FILE_CONTENT, len(content), len(TEST_FILE_CONTENT));
    }else{
        t.Logf("Read %d bytes from %q. OK\n", len(content), TEST_FILE_PATH);
    }
}

//Test the ensure dir function
func testEnsureDir(t *testing.T){
    reqs:=[]struct{req *Request; errorExpected bool}{
        {
            NewRequest("ensureDir", TEST_FILE_PATH),
            true, //It's a file
        },
        {
            NewRequest("ensureDir", TEST_DIR_PATH),
            false,
        },
        {
            NewRequest("ensureDir", TEST_DIR_PATH),
            false, //Already created and it's a dir
        },
    };
    for _,rq:=range(reqs){
        req:=rq.req;
        ioreq<-req;
        if err:=<-req.Status; (err != nil) != rq.errorExpected {
            t.Errorf("Unexpected (not)error %s\n", err);
        }else{
            t.Logf("Expected result error: %s\n", err);
        }
    }
}

//Test update times. Check them after with stat
func testUpdateTimes(t *testing.T){
    rq:=NewRequest("updateTimes", TEST_FILE_PATH, "", TEST_FILE_TIME);
    srq:=NewRequest("stat", TEST_FILE_PATH);
    srq2:=NewRequest("stat", TEST_FILE_PATH);
    rq2:=NewRequest("updateTimes", TEST_FILE_PATH, TEST_FILE_TIME, TEST_FILE_TIME);
    tm,err:=time.Parse(time.RFC3339, TEST_FILE_TIME);
    if err != nil {
        t.Error(err.Error());
        return;
    }
    ioreq<-rq;
    if err:=<-rq.Status; err != nil {
        t.Error(err.Error());
        return;
    }
    ioreq<-srq;
    if err:=<-srq.Status; err != nil {
        t.Error(err.Error());
        return;
    }else if v,ok:=srq.ReturnValue.(*os.FileInfo); ok {
        if mt:=(*v).ModTime(); !mt.Equal(tm) {
            t.Errorf("Times aren't equal. Time on file: %s, set time: %s\n", mt.Format(time.RFC3339), tm.Format(time.RFC3339));
        }
    }else{
        t.Errorf("Unexpected return type from stat. Got %T\n", srq.ReturnValue);
        return;
    }
    ioreq<-rq2;
    if err:=<-rq2.Status; err != nil {
        t.Error(err.Error());
        return;
    }
    ioreq<-srq2;
    if err:=<-srq2.Status; err != nil {
        t.Error(err.Error());
        return;
    }else if v,ok:=srq2.ReturnValue.(*os.FileInfo); ok {
        if mt:=(*v).ModTime(); !mt.Equal(tm) {
            t.Errorf("Times aren't equal. Time on file: %d, set time: %d\n", mt.Unix(), tm.Unix());
        }
    }else{
        t.Errorf("Unexpected return type from stat. Got %T\n", srq.ReturnValue);
        return;
    }
}

//Test stat function return value
func testStat(t *testing.T){
    rqs:=[]*Request{
        NewRequest("stat", TEST_FILE_PATH),
        NewRequest("stat", TEST_DIR_PATH),
    };
    for _,req:=range(rqs){
        ioreq<-req;
        if err:=<-req.Status; err != nil {
            t.Error(err.Error());
        }else if _,ok:=req.ReturnValue.(*os.FileInfo); !ok {
            t.Errorf("Bad return value type %T\n", req.ReturnValue);
        }
    }
}

//Test setXattr
func testSetXattr(t *testing.T){
    req:=NewRequest("setXattr", TEST_FILE_PATH, TEST_XATTR_CONTENT);
    ioreq<-req;
    if err:=<-req.Status; err != nil {
        t.Error(err.Error());
    }
}

//Test getXattr
func testGetXattr(t *testing.T){
    req:=NewRequest("getXattr", TEST_FILE_PATH);
    ioreq<-req;
    if err:=<-req.Status; err != nil {
        t.Error(err.Error());
    }else if v,ok:=req.ReturnValue.(string); ok {
        if v != TEST_XATTR_CONTENT {
            t.Errorf("Xattr on file was %q, but expected %q\n", v, TEST_XATTR_CONTENT);
        }else{
            t.Logf("Got %q from xattr as expected\n", v);
        }
    }else{
        t.Errorf("Bad return type from getXattr %T\n", req.ReturnValue);
    }
}

//Test rename call
func testRename(t *testing.T){
    req:=NewRequest("rename", TEST_FILE_PATH, TEST_FILE_PATH2);
    t.Logf("Usign request %+v\n", req);
    ioreq<-req;
    if err:=<-req.Status; err != nil {
        t.Error(err.Error());
    }else{
        t.Logf("Renamed %q to %q\n", TEST_FILE_PATH, TEST_FILE_PATH2);
    }
}

//Test remove call (recursive)
func testRemove(t *testing.T){
    reqs:=[]struct{rq *Request; errorExpected bool}{
        {NewRequest("remove", TEST_FILE_PATH), false}, //Already removed
        {NewRequest("remove", ""), true}, //As a protection mechanism, reject removing the root path with an empty string
        {NewRequest("remove", "/"), false}, //Remove root as a final step
    }
    for _,rq:=range(reqs){
        ioreq<-rq.rq;
        if err:=<-rq.rq.Status; (err!=nil)!=rq.errorExpected{
            t.Errorf("Unexpected (not) error %s\n", err);
        }else{
            t.Logf("Expected result from remove function: %s\n", err);
        }
    }
}
