package localfs;

import(
    "context"
    "path/filepath"
    "github.com/fsnotify/fsnotify"
  . "gitlab.com/mrvik/gogdrive/types"
    "math/rand"
    "os"
    "time"
)

const (
    //Max thread sleep in milliseconds
    MAX_SLEEP=50;
)

var(
    watcher *fsnotify.Watcher;
    eventList=new(MutexedLinkedList);
)

//Event watcher. Prepared to run on a separate goroutine. Will block until it gets a message from exit channel.
//Events are added to a shared list.
//
//Some clarification notes here:
//- When a large directory hierarchy is added, some files won't fire an event, but the entire folder will get uploaded.
//- Directories that didn't trigger an event are still added to watch thanks to using Walk, so changes to files inside will still trigger an event
//Example:
/*
```
$ mkdir -p asd/qweqwe{01..10}/xcvbxvb{01..10}
$ #This will trigger a lot of events, but won't trigger an event for all of them
$ touch asd/qweqwe{01..10}/xcvbxvb{01..10}/testing
$ #Gogdrive didn't fire a event for each file, but it added everything to watcher, so this time all the files get an event
```
*/
func watchEvents(ctx context.Context, watcher *fsnotify.Watcher){
    var exit bool;
    defer wg.Done();
    defer watcher.Close();
    if blacklistReady != nil { //Wait until the blacklist is created and set
        blacklistReady.Wait();
    }
    for !exit {
        select{
        case <-ctx.Done():
            exit=true;
        case v,ok:=<-watcher.Events:
            if !ok {
                panic("Tried to read from a closed channel. Event channel was closed at this time");
            }else{
                eventList.Put(v);
                //Sleep an arbitrary amount of time to prevent starvating
                time.Sleep(time.Millisecond*time.Duration(rand.Int63()%MAX_SLEEP));
            }
        case v,ok:=<-watcher.Errors:
            if !ok {
                panic("Tried to read from a closed channel. Error channel was closed at this time");
            }else{
                eventList.Put(v);
            }
        }
    }
}

//This function starts fsnotify watcher and sets the watcher global variable.
func startWatcher(ctx context.Context) error{
    wt,err:=fsnotify.NewWatcher();
    if err != nil {
        return err;
    }
    watcher=wt;
    err=filepath.Walk(localdir, addFromWalk);
    if err != nil{
        return err;
    }
    //Start listening to local events
    wg.Add(1);
    go watchEvents(ctx, wt);
    return nil;
}

//This function adds files to watcher. Is invoked by filepath.Walk
func addFromWalk(path string, info os.FileInfo, err error) error{
    if err == nil {
        if info.IsDir() {
            return watcher.Add(path);
        }
    }else{
        log.Errorf("Not watching %s due to error: %s\n", path, err);
    }
    return nil;
}
