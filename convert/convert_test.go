package convert

import (
    "strings"
  . "gitlab.com/mrvik/gogdrive/types"
    "gitlab.com/mrvik/gogdrive/util"
    "gitlab.com/mrvik/logger/testAdapter"
  drv "google.golang.org/api/drive/v3"
    "testing"
)

var (
    tree *Tree;
    files=[]struct{file *drv.File; path string; errorExpected bool}{
        {
            &drv.File{
                Parents: []string{"1234"},
                Id: "001",
                Name: "Testing before the parent",
            },
            "/testing/Testing before the parent",
            false,
        },
        {
            &drv.File{
                Parents: []string{"123"},
                Id: "999",
                Name: "fileOnRoot",
            },
            "/fileOnRoot",
            false,
        },
        {
            &drv.File{
                Parents: []string{"123"},
                Id: "1234",
                Name: "testing",
                MimeType: util.DRIVE_FOLDER_MIME,
            },
            "/testing",
            false,
        },
        {
            &drv.File{
                Parents: []string{"1234"},
                Id: "111",
                Name: "subtest",
            },
            "/testing/subtest",
            false,
        },
        {
            &drv.File{
                Parents: []string{"1234"},
                Id: "112",
                Name: "Another",
            },
            "/testing/Another",
            false,
        },
        {
            &drv.File{ //Error expected as the file should exist
                Parents: []string{"1234"},
                Id: "112",
                Name: "Another",
            },
            "/testing/Another",
            true,
        },
        {
            &drv.File{ //Should expect an error as path changes, but id exists
                Parents: []string{"1234"},
                Id: "112",
                Name: "AnotherOne",
            },
            "/testing/AnotherOne",
            true,
        },
        {
            &drv.File{ //Should expect an error because path exists
                Parents: []string{"1234"},
                Id: "222",
                Name: "Another",
            },
            "/testing/Another",
            true,
        },
    }
    earlyRegisterFiles=[]struct{id, path, expectedParent string; errorExpected bool}{
        {
            "1234",
            "/anotherNonExisting path",
            "", //No parent expected
            true, //The ID already exists
        },
        {
            "qwe123",
            "/testing/something",
            "1234",
            false,
        },
        {
            "qwe123",
            "/testing/repeated",
            "",
            true, //Duplicate file. Cannot be added twice
        },
        {
            "qwe1234",
            "/testing/something/someFile",
            "qwe123",
            false, //We don't know the type of testing/something so we can't assume it's not a dir
        },
    }
    events=[]struct{evts []*Event; err int}{
        {
            []*Event{nil}, //Nil events fire an error
            0,
        },
        {
            []*Event{
                {Type: "REMOVE", File: &drv.File{Id: "123", Name: "My Drive"}, Path: "/"},
            },
            0, //Should refuse to remove root
        },
        {
            []*Event{
                {Type: "WRITE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
            },
            0, //Should fail to update a non-existent file
        },
        {
            []*Event{
                {Type: "CREATE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
                {Type: "RENAME", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: ""},
                {Type: "REMOVE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: "", OldPath: "/test1"},
            },
            2, //File /test1 shouldn't exist as it's been moved away.
        },
        {
            []*Event{
                {Type: "CREATE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
                {Type: "RENAME", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: ""},
                {Type: "REMOVE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: "", OldPath: "/test2"},
            },
            -1,
        },
        {
            []*Event{
                {Type: "CREATE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
                {Type: "RENAME", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: ""},
                {Type: "WRITE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
                {Type: "REMOVE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: "", OldPath: "/test2"},
            },
            2, //Should fire an error becouse path is empty and name is old
        },
        {
            []*Event{
                {Type: "CREATE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
                {Type: "RENAME", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: ""},
                {Type: "WRITE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: "/test1"},
                {Type: "REMOVE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: "", OldPath: "/test2"},
            },
            2, //Should fire an error becouse path is the old one
        },
        {
            []*Event{
                {Type: "CREATE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
                {Type: "RENAME", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: ""},
                {Type: "WRITE", File: &drv.File{Id: "1233", Parents: []string{"123"}, Name: "test1"}, Path: "/test1"},
                {Type: "REMOVE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: "", OldPath: "/test2"},
            },
            2, //Should fire an error becouse FileID doesn't exist
        },
        {
            []*Event{
                {Type: "CREATE", File: &drv.File{Id: "111", Parents: []string{"123"}, Name: "myfolder"}, Path: ""},
                {Type: "CREATE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
                {Type: "RENAME", File: &drv.File{Id: "1234", Parents: []string{"111"}, Name: "test2"}, Path: ""},
                {Type: "CHMOD", File: &drv.File{Id: "1234", Parents: []string{"111"}, Name: "test2"}, Path: "/myfolder/test2"}, //Test the CHMOD event. See comment on ConsumeEvent
                {Type: "WRITE", File: &drv.File{Id: "1234", Parents: []string{"111"}, Name: "test2"}, Path: "/myfolder/test2"},
                {Type: "REMOVE", File: &drv.File{Id: "1234", Parents: []string{"111"}, Name: "test2"}, Path: "", OldPath: "/myfolder/test2"},
                {Type: "REMOVE", File: &drv.File{Id: "111", Parents: []string{"123"}, Name: "myfolder"}, Path: "", OldPath: "/myfolder"},
            },
            -1, //No error allowed
        },
        {
            []*Event{
                {Type: "CREATE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test1"}, Path: ""},
                {Type: "RENAME", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: ""},
                {Type: "CHMOD", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: "/test2"}, //Test the CHMOD event. See comment on ConsumeEvent
                {Type: "WRITE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: "/test2"},
                {Type: "REMOVE", File: &drv.File{Id: "1234", Parents: []string{"123"}, Name: "test2"}, Path: "", OldPath: "/test2"},
            },
            -1, //No error allowed
        },
    }
    selected=8; //Should be a slice of events that leaves the tree empty
);

//CONVERSION TREE TEST (INTEGRATION TEST)
func TestConversionTree(t *testing.T){
    lgr:=testAdapter.CreateTestAdapter(t);
    Init(lgr);
    ct,err:=NewTree("123", []*drv.File{});
    if err != nil {
        t.Fatal(err);
    }else{
        tree=ct;
        t.Log("Tree created OK");
    }
    t.Run("add", testAdd);
    t.Run("query", testQuery);
    t.Run("earlyRegister", testRegisterFids);
    t.Run("remove", testRemove);
    var ln1, ln2 int;
    if ln1=len(*tree.fileIdMap); ln1 > 0 {
        t.Errorf("FileID Map is not empty! Has %d elements\n\t%+v\n", ln1, *tree.fileIdMap);
    }else{
        t.Log("FileID Map is OK");
    }
    if ln2=len(*tree.pathMap); ln2 > 0 {
        t.Errorf("Path Map not empty! Has %d elements\n\t%v\n", ln2, *tree.pathMap);
    }else{
        t.Log("Path Map is OK");
    }
    if ln1 != ln2 {
        t.Errorf("Maps are out of sync!! FileID has %d elements and PathMap has %d\n", ln1, ln2);
    }
    tree=nil; //Teardown
}

//Test Add function and genPath
func testAdd(t *testing.T){
    for k,test:=range(files){
        file:=NewFile(test.file, test.path);
        np,err:=tree.Add(file);
        if err != nil {
            if !test.errorExpected {
                t.Error(err.Error());
            }else{
                t.Logf("Errored as expected: %s\n", err);
                continue;
            }
        }else if test.errorExpected {
            t.Error("Add didn't error as expected");
        }
        if np != "" && np != test.path {
            t.Errorf("Different path from Add %q but expected %q for %d\n", np, test.path, k);
        }else{
            t.Logf("%d: File %q added OK\n", k, np);
        }
        f,err:=tree.GetFileByPath(test.path);
        if err != nil {
            t.Errorf("Error getting added file %q: %s\n", test.path, err);
        }else if f == nil {
            t.Errorf("File is nil after adding it on %d. Selected by path %q\n", k, test.path);
        }else if f != file{
            t.Errorf("Query returned a different file %p but expected %p\n", f, file);
        }else{
            t.Logf("Query for %q(%d) OK\n", test.path, k);
        }
    }
}

func testQuery(t *testing.T){
    for _,f:=range(files){
        if f.errorExpected {
            continue; //Avoid this files
        }
        file,err:=tree.GetFileByPath(f.path);
        if err != nil {
            t.Errorf("Error getting file %q: %s\n", f.path, err);
        }else if file.Id != f.file.Id {
            t.Errorf("Files aren't the same: got %q but expected ID %q\n", file.Id, f.file.Id);
        }else{
            t.Logf("Test over %q(%s) OK\n", f.path, file.Id);
        }
    }
}

func testRemove(t *testing.T){
    //Test existent file and bad path
    err:=tree.Remove(files[0].file.Id, "not-existent");
    if err == nil {
        t.Error("Tried to remove a file with ID and ")
    }
    for _,f:=range(files){
        if f.errorExpected {
            continue; //Avoid errored files
        }
        err=tree.Remove(f.file.Id, f.path);
        if err != nil {
            t.Error(err.Error());
        }else if fl,err:=tree.GetFileByPath(f.path); err == nil {
            t.Errorf("GetFileByPath didn't return a error and file is %+v\n", fl);
        }else if fl,err=tree.GetFileByID(f.file.Id); err == nil {
            t.Errorf("GetFileByID didn't return a error and file is %+v\n", fl);
        }else{
            t.Logf("File %s was removed OK\n", f.path);
        }
    }
    for _,f:=range(earlyRegisterFiles){
        if f.errorExpected{
            continue;
        }
        err=tree.Remove(f.id, f.path);
        if err != nil{
            t.Error(err);
        }
    }
}

func testRegisterFids(t *testing.T){
    for _,f:=range(earlyRegisterFiles){
        t.Logf("Starting test for %+v\n", f);
        err:=tree.RegisterIDForPath(f.id, f.path);
        if hasError:=err!=nil; hasError != f.errorExpected{
            t.Errorf("Expected error (%t) but got %s\n", f.errorExpected, err);
            continue;
        }else if f.errorExpected{
            continue;
        }
        registered, err:=tree.GetFileByID(f.id);
        if err != nil{
            t.Error(err);
            continue;
        }
        if actualParent:=registered.Parents[0]; actualParent != f.expectedParent{
            t.Errorf("Expected to have %q as parent but got %q. Life is hard\n", f.expectedParent, actualParent);
        }
    }
}

//Test event listener and final state after event
func TestConsumeEvent(t *testing.T){
    var (
        err error;
        errored int;
    )
    for snum,slice:=range(events){
        tree,err=NewTree("123", []*drv.File{}); //Create a new tree for each bunch of events
        errored=-1;
        if err != nil {
            t.Fatalf("Error creating tree: %s\n", err);
        }
        for num,evt:=range(slice.evts){
            if evt != nil && evt.File != nil {
                evt.FileID=evt.File.Id;
            }
            err=tree.ConsumeEvent(evt);
            if err != nil {
                errored=num;
                break;
            }
        }
        if errored != slice.err {
            if errored >= 0 {
                t.Errorf("Slice %d: Expected to error on %d, but errored on %d.\n\tEvent: %+v\n\tError: %s", snum, slice.err, errored, slice.evts[errored], err);
            }else{
                t.Errorf("Slice %d: Expected to error on %d, but didn't", snum, slice.err);
            }
        }
        ln1:=len(*tree.fileIdMap);
        ln2:=len(*tree.pathMap);
        if ln1 != ln2 {
            t.Errorf("Pass %d: Maps are out of sync. FileID map has %d elements and path map %d\n", snum, ln1, ln2);
        }else{
            var s string;
            if ln1 != 1 {
                s="s";
            }
            t.Logf("Maps are synced. Both have %d element%s\n", ln1, s);
        }
        if slice.err != -1 {
            continue; //Skip empty checks
        }
        if ln1 != 0 {
            t.Errorf("Pass %d: FileID map has %d elements. 0 Expected\n", snum, ln1);
        }
        if ln2 != 0 {
            t.Errorf("Pass %d: Path map has %d elements. 0 Expected\n", snum, ln2);
        }
    }
}

//Test the DiffParents function
func TestDiffParents(t *testing.T){
    diffFiles:=[]struct{oldpath, newpath string; oldParents, newParents string; errorExpected bool}{
        {
            "/testing/subtest",
            "/testing/subtest",
            "",
            "",
            false,
        },
        {
            "/testing/subtest",
            "/testing/modifiedName",
            "",
            "",
            false,
        },
        {
            "/testing/subtest",
            "/subtest",
            "1234",
            "123",
            false,
        },
        {
            "/testing",
            "/othertesting",
            "",
            "",
            false,
        },
        {
            "/fileOnRoot",
            "/testing/some",
            "123",
            "1234",
            false,
        },
        {
            "/notExisting",
            "/someother/path", //Note that the error will be here because the second path doesn't exist. The first one is used for comparison only
            "",
            "",
            true,
        },
    }
    for index,f:=range(diffFiles){
        tree,err:=createTree();
        if err != nil {
            t.Fatal(err.Error());
        }
        newp,oldp,err:=tree.DiffParents(f.newpath, f.oldpath);
        if err!=nil{
            if !f.errorExpected{
                t.Errorf("Unexpected error on slice %d: %s\n", index, err);
                continue;
            }else{
                t.Logf("Got error on %d as expected: %s\n", index, err);
            }
        }else{
            if f.errorExpected{
                t.Errorf("Test %d: Expected error but got nothing\n", index);
                continue;
            }else{
                t.Logf("Test %d: Didn't get error as expected\n", index);
            }
        }
        if oldp != f.oldParents {
            t.Errorf("Test %d: Old parents were %q but expected %q\n", index, oldp, f.oldParents);
        }else{
            t.Logf("Test %d: Old parents were %q as expected\n", index, oldp);
        }
        if newp != f.newParents{
            t.Errorf("Test %d: New parents were %q but expected %q\n", index, newp, f.newParents);
        }else{
            t.Logf("Test %d: New parents were %q as expected\n", index, newp);
        }
        //TODO compare resulting parents and insert a continue after errors
    }
}

//Test GenerateParents function
func TestGenerateParents(t *testing.T){
    tree,err:=createTree();
    if err != nil {
        t.Fatal(err.Error());
    }
    cases:=[]struct{path string; expectedParents string; errorExpected bool}{
        {
            "/testing/something",
            "1234",
            false,
        },
        {
            "/notExistent",
            "123",
            false, //Parent exists
        },
        {
            "/notExistent/someFile",
            "",
            true,
        },
    }
    for index,file:=range(cases){
        parents, err:=tree.GenerateParents(file.path);
        if err != nil {
            if !file.errorExpected {
                t.Errorf("Test %d: Unexpected error %s\n", index, err);
                continue;
            }else{
                t.Logf("Test %d errored as expected: %s\n", index, err);
            }
        }else{
            if file.errorExpected {
                t.Errorf("Test %d: Expected error but got nothing\n", index);
                continue;
            }else{
                t.Logf("Test %d: Didn't get any error as expected\n", index);
            }
        }
        if merged:=strings.Join(parents, ","); merged != file.expectedParents {
            t.Errorf("Test %d: Expected parents to be %q but got %q\n", index, file.expectedParents, merged);
        }else{
            t.Logf("Test %d: Parents returned expected value %q\n", index, merged);
        }
    }
}

//Create tree for TestDiffParents
func createTree() (*Tree, error){
    tree,err:=NewTree("123", []*drv.File{});
    if err != nil {
        return nil,err;
    }
    for _,f:=range(files){
        if !f.errorExpected {
            _,err:=tree.Add(NewFile(f.file, f.path));
            if err != nil {
                return nil,err;
            }
        }
    }
    return tree,nil;
}

//Benchmark the consume event function
func BenchmarkConsumeEvent(b *testing.B){
    lgr:=testAdapter.CreateTestAdapter(b);
    Init(lgr);
    ct,err:=NewTree("123", []*drv.File{});
    if err != nil {
        b.Fatal(err);
    }
    b.ResetTimer();
    for i:=0; i < b.N; i++ {
        for _,event:=range(events[selected].evts){
            err=ct.ConsumeEvent(event);
            if err != nil {
                b.Error(err);
            }
        }
    }
}
