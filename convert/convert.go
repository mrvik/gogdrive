package convert;

import(
    "fmt"
    "strings"
    "path"
    "sync"
  . "gitlab.com/mrvik/logger"
  . "gitlab.com/mrvik/gogdrive/types"
    "gitlab.com/mrvik/gogdrive/util"
  drv "google.golang.org/api/drive/v3"
)

const(
    ORPHAN_ROOT="/drive-lost+found-here/";
)

var(
    log Logger;
)

//This function initializes global vars
func Init(lg Logger){
    log=lg;
}

//This type holds the entire file hierarchy and its equivalent on the local filesystem
//As you may know, Google Drive doesn't use file paths, so it's difficult to trace a file to it's path here.
//We want to hace the following info indexed:
//- Path
//- FileID
//So will make two maps, one where the key is the path and another where the key is the fileID. Value is a pointer to File
//The value of path map will be a double pointer
//Maps are pointers to allow atomic updates
type Tree struct{
    fileIdMap *map[string]*File;
    pathMap *map[string]**File;
    mutex *sync.RWMutex;
    rootID string;
}

//Initialize a new Tree
func NewTree(root string, files []*drv.File) (*Tree,error){
    rt:=&Tree{
        fileIdMap: new(map[string]*File),
        pathMap: new(map[string]**File),
        mutex: new(sync.RWMutex),
        rootID: root,
    };
    err:=rt.UpdateTree(files);
    return rt,err;
}

//Update maps on the Tree. The update is atomic, so if an error occurs the maps are left untouched
func (tree *Tree) UpdateTree(files []*drv.File) error{
    //Maps are created from scratch, so old and stale files are not still here after an update
    fidm:=make(map[string]*File);
    pam:=make(map[string]**File);
    //Loops are separated becouse we need fidmap to be populated before looking for IDs there
    for _,file:=range(files) {
        if file==nil {
            continue; //May contain nil files (killed orphans)
        }
        fidm[file.Id]=NewFile(file, ""); //Path is not populated as it hasn't been generated
    }
    for _,file:=range(files) {
        if file==nil {
            continue; //May contain nil files (killed orphans)
        }
        pth:=genPath(tree.rootID, file, fidm);
        log.Debugf("Generated %q for %q\n", pth, file.Name);
        if pth != "" {
            //Duplicate files are allowed by Google Drive. This situation must be resolved before staring gogdrive(1)
            //Check the FAQs page for reasons on why gogdrive(1) won't try to resolve this conflicts
            if f,exists:=pam[pth]; exists && (*f).Id != file.Id { //Be quiet if Id is the same
                log.Errorf("Duplicate entry for %s while creating tree!\n\tCurrent file: %s\n\tExisting one: %s\n", pth, file.Id, (*f).Id);
                log.Debugf("\n\tCURRENT FILE(Trashed: %t): %s\n\tEXISTING FILE(Trashed: %t): %s\n", file.Trashed, file, (*f).Trashed, (*f).File);
                panic("File "+pth+" is duplicated. Resolve this situation on drive.google.com before staring gogdrive");
            }
            if f,ok:=fidm[file.Id]; ok {
                f.Path=pth;
                pam[pth]=&f;
            }
        }
    }
    log.Infof("Remote file map has %d entries\n", len(fidm));
    tree.mutex.Lock();
    defer tree.mutex.Unlock();
    tree.fileIdMap,tree.pathMap=&fidm,&pam;
    return nil;
}

func (tree *Tree) Print(){
    tree.mutex.RLock();
    defer tree.mutex.RUnlock();
    var str string;
    for path,file:=range(*tree.pathMap) {
        str+=path+"\t"+(**file).Id+"\n";
    }
    log.Infof("Actual tree: \n%s\n", str);
}

//Gets a file and the fileID hierarchy to generate a valid path
//As you may have noticed, a drive file can have more than one parent, so we'll generate the path for the first element on array
//
//One special case is when the element has no parents or they cannot be discovered. This will trigger a warning and skip the file.
//Error will be nil and path zero-valued
func genPath(root string, file *drv.File, fidmap map[string]*File) string{
    var chain string;
    actualElement:=NewFile(file, ""); //Create a file w/o path for first time
    for {
        chain=path.Join(actualElement.Name,chain);
        if len(actualElement.Parents) < 1 {
            orphanString:=orphanStringFor(actualElement);
            log.Warnf("%q has no parents. Converted to %s\n", chain, orphanString);
            return orphanString;
        }
        parent:=actualElement.Parents[0];
        if parent == root {
            break;
        } else if parent == "" {
            log.Warnf("%q has no parents. The entire family is orphan\n", chain);
            return "";
        }
        var ok bool;
        actualElement,ok=fidmap[parent];
        if !ok {
            log.Warnf("Can't find file with ID %q. Assuming the file %q is orphan\n", parent, chain);
            return "";
        }
    }
    return "/"+chain; //Add the first slash
}

//Create a path on root for orphan items
func orphanStringFor(file *File) string{
    return path.Join(ORPHAN_ROOT, file.Name+"__"+file.Id);
}

//Diff parents and output strings to be passed to a files update call. Needs the newPath, oldPath and original parents
func (tree *Tree) DiffParents(newPath, oldPath string) (newParents, oldParents string, rterr error){
    if !util.SameBase(newPath, oldPath) {
        original,err:=tree.GetFileByPath(oldPath);
        if err != nil {
            return "","",err;
        }
        oldParents=strings.Join(original.File.Parents, ",");
        newParent:=path.Dir(newPath);
        el,err:=tree.GetFileByPath(newParent);
        if err != nil {
            rterr=err;
            return;
        }
        newParents=el.File.Id;
    }
    return;
}

//Generate a parents slice from a file path
func (tree *Tree) GenerateParents(pth string) (parents []string, err error){
    parent,err:=tree.GetFileByPath(path.Dir(pth));
    if err != nil {
        return;
    }
    parents=[]string{parent.File.Id};
    return;
}

//This function takes a file ID and returns a pointer to drv.File or nil if the file doesn't exist
func (tree *Tree) GetFileByID(id string) (*File, error){
    if id == tree.rootID {
        return NewFile(&drv.File{Id: tree.rootID}, "/"), nil;
    }
    tree.mutex.RLock();
    defer tree.mutex.RUnlock();
    f,ok:=(*tree.fileIdMap)[id];
    if !ok{
        return nil,NewFileNotExistError("",id);
    }
    return f,nil;
}

func (tree *Tree) GetFileByPath(path string) (*File, error){
    if path == "/" { //Return a fake file for Root
        return NewFile(&drv.File{Id: tree.rootID}, "/"), nil;
    }
    tree.mutex.RLock();
    defer tree.mutex.RUnlock();
    f,ok:=(*tree.pathMap)[path];
    if !ok{
        return nil,NewFileNotExistError(path, "");
    }else{
        return *f, nil;
    }
}

//Generate a path returning an error if path is empty
func genPathErr(rootId string, file *drv.File, tree map[string]*File) (newPath string, returnedError error){
    newPath=genPath(rootId, file, tree);
    if newPath == "" {
        returnedError=NewPathError("Can't generate path for %q(%s)", file.Name, file.Id);
    }
    return;
}
//Insert a new file in the tree (both maps). Returns error if that key already exists.
//Also it generates a path for the file if it doesn't have one. Returns the generated path or the one included on File (a rare case).
func (tree *Tree) Add(file *File) (newPath string, rterr error){
    tree.mutex.Lock();
    defer tree.mutex.Unlock();
    if file.Path == "" {
        newPath,rterr=genPathErr(tree.rootID, file.File, *tree.fileIdMap);
    } else {
        newPath=file.Path;
    }
    if rterr != nil {
        return;
    }
    _,idExists:=(*tree.fileIdMap)[file.Id];
    _,pathExists:=(*tree.pathMap)[newPath];
    if idExists||pathExists {
        rterr=NewFileExistsError(newPath, file.Id);
    } else { //Ids doesn't exist
        file.Path=newPath;
        (*tree.fileIdMap)[file.Id]=file; //This is safe because tree is locked
        (*tree.pathMap)[newPath]=&file;
    }
    return;
}

//Register a new File with the bare minimum (FileID, name, path and parents)
func (tree *Tree) RegisterIDForPath(id, filepath string) error{
    if id == "" || filepath == "" {
        return NewArgumentsError("Path ot file ID are empty");
    }
    parent, err:=tree.GetFileByPath(path.Dir(filepath));
    if err != nil{
        return err;
    }
    file:=drv.File{
        Id: id,
        Name: path.Base(filepath),
        Parents: []string{parent.Id},
    };
    log.Debugf("Adding file entry for id %q and path %q\n", id, filepath);
    np,err:=tree.Add(NewFile(&file, filepath));
    if err != nil{
        return err;
    }
    if np != filepath {
        return fmt.Errorf("provided and generated paths missmatch. Got %q but %q was generated", filepath, np);
    }
    return nil;
}

//Update a file in the tree. Optionally generate a new path which will be returned.
//If the requested file doesn't exist on the tree, an error will be returned
func (tree *Tree) Update(newFile *drv.File, regeneratePath bool) (newPath string, rterr error){
    tree.mutex.Lock();
    defer tree.mutex.Unlock();
    entry,exists:=(*tree.fileIdMap)[newFile.Id];
    if !exists {
        return "",NewFileNotExistError(newFile.Name, newFile.Id);
    }
    var updateMap bool;
    if regeneratePath {
        newPath,rterr=genPathErr(tree.rootID, newFile, *tree.fileIdMap);
        if rterr!=nil{
            return;
        }
        if newPath != entry.Path {
            delete(*tree.pathMap, entry.Path); //Delete old entry
            updateMap=true;
        }
    } else {
        if newFile.Name != entry.Name {
            return "",NewArgumentsError("Can't change file name w/o regenerating path!");
        }
        newPath=entry.Path;
    }
    if newPath == "" {
        return "",NewArgumentsError("Path can't be empty on call to Update. RegeneratePath was set to %t", regeneratePath);
    }
    nf:=NewFile(newFile, newPath);
    (*tree.fileIdMap)[newFile.Id]=nf;
    if updateMap { //Avoid updating map if key doesn't change
        (*tree.pathMap)[newPath]=&nf;
    }
    return;
}

//Remove a file from the trees
func (tree *Tree) Remove(id, oldPath string) (err error){
    tree.mutex.Lock();
    defer tree.mutex.Unlock();
    _,idExists:=(*tree.fileIdMap)[id];
    _,pathExists:=(*tree.pathMap)[oldPath];
    if !idExists || !pathExists {
        err=NewFileNotExistError(oldPath, id);
    }else{
        delete(*tree.fileIdMap, id);
        delete(*tree.pathMap, oldPath);
    }
    return;
}

//This function consumes a remote event to update the tree
func (tree *Tree) ConsumeEvent(event *Event) (err error){
    if event == nil {
        return NewArgumentsError("Cannot process a nil event");
    }
    switch event.Type {
    case "CREATE":
        var np string;
        np,err=tree.Add(NewFile(event.File, event.Path)); //Also generates a path for the file and event
        event.Path=np;
    case "RENAME":
        var np string;
        np,err=tree.Update(event.File, true);
        event.Path=np;
    case "REMOVE":
        err=tree.Remove(event.FileID, event.OldPath);
    case "WRITE", "CHMOD": //CHMOD event isn't emited from Drive, but maybe in the future it is
        _,err=tree.Update(event.File, false);
    default:
        err=NewUnimplementedType(event.Type);
    }
    return;
}
