//+build linux freebsd openbsd netbsd

package util;

import (
    "os"
    "syscall"
    "time"
)

func GetAtime(stat os.FileInfo) (rt time.Time) {
    if stat != nil {
        if val,ok:=stat.Sys().(*syscall.Stat_t); ok{
            rt=time.Unix(getSecNsec(val));
        }
    }
    return;
}
