package util

import (
    "syscall"
)

func getSecNsec(stat *syscall.Stat_t) (int64, int64){
    return stat.Atim.Unix();
}
