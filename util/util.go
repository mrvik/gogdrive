//Util functions
package util;

import(
    "time"
    "path"
    "regexp"
)

//MIME Type of drive folders
const DRIVE_FOLDER_MIME="application/vnd.google-apps.folder";

var (
    //Regular expression to test mime types against
    IsGAppReg=regexp.MustCompile(`(application\/vnd\.google-apps)\.([a-zA-Z-]+)`); //Use a raw string to avoid issues with backslashes
)
//Return true or false depending on the mime type is from google apps
func IsGoogleApp(mime string) bool{
    return IsGAppReg.MatchString(mime);
}

//Return true or false depending if the mime type equals DRIVE_FOLDER_MIME
//Note that this function is not being tested. No need to check an if statement
func IsDir(mime string) bool{
    return mime==DRIVE_FOLDER_MIME;
}

//Take the first non-empty string from parameters. If none, return an empty string
func ChooseANonEmptyString(strings ...string) string{
    for _,v:=range(strings) {
        if v != "" {
            return v;
        }
    }
    return "";
}

//Return true or false if the two files have a different base directory. Assuming those are paths
func SameBase(p1, p2 string) bool{
    b1:=path.Dir(p1);
    b2:=path.Dir(p2);
    return b1==b2;
}

//Compare dates. True if first date is after second date or equals. False otherwise
func TimeAfterOrEq(first, second time.Time) bool{
    t1:=first.Unix();
    t2:=second.Unix();
    return t1>=t2;
}

