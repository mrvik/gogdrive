package util;

import (
    "testing"
    "time"
);

func TestIsGoogleApp(t *testing.T){
    inout:=[4]struct{string;bool}{
        {
            "application/tar",
            false,
        },
        {
            DRIVE_FOLDER_MIME,
            true,
        },
        {
            "application/vnd.notgoogle.thing",
            false,
        },
        {
            "application/vnd.google-apps.drawing",
            true,
        },
    }
    for _,v:=range(inout){
        res:=IsGoogleApp(v.string);
        if res != v.bool {
            t.Errorf("Got %t for MIME %s (expected to be %t)\n", res, v.string, v.bool);
        } else {
            t.Logf("Got %t for MIME %s. OK\n", res, v.string);
        }
    }
}

func TestChooseANonEmptyString(t *testing.T){
    argsRsp:=[]struct{args []string; result string}{
        {
            []string{"", "nonempty", "another"},
            "nonempty",
        },
        {
            []string{"some", "string", "nonempty"},
            "some",
        },
    }
    for _,v:=range(argsRsp){
        res:=ChooseANonEmptyString(v.args...);
        if res != v.result {
            t.Errorf("Chosen %s from %v. Expected %s\n", res, v.args, v.result);
        }else{
            t.Logf("Chosen %s from %v. OK\n", res, v.args);
        }
    }
}

func TestSameBase(t *testing.T){
    args:=[]struct{args [2]string; res bool}{
        {
            [2]string{"/relative/to/here", "/not/the/same"},
            false,
        },
        {
            [2]string{"/relative/to/here", "/relative/to/not/here"},
            false,
        },
        {
            [2]string{"/some/relative/path", "/some/relative/another-path"},
            true,
        },
        {
            [2]string{"/another/same", "/another/same"},
            true,
        },
    }
    for _,v:=range(args){
        res:=SameBase(v.args[0], v.args[1]);
        if res != v.res {
            t.Errorf("%s and %s same base %t. Expected %t\n", v.args[0], v.args[1], res, v.res);
        }else{
            t.Logf("%s and %s, same base: %t. OK\n", v.args[0], v.args[1], res);
        }
    }
}

func TestTimeAfterOrEq(t *testing.T){
    args:=[]struct{times [2]time.Time; result bool}{
        {
            [2]time.Time{time.Unix(1000, 0), time.Unix(1000, 80)},
            true,
        },
        {
            [2]time.Time{time.Unix(1000, 0), time.Unix(1000, 0)},
            true,
        },
        {
            [2]time.Time{time.Unix(1001, 0), time.Unix(1000, 0)},
            true,
        },
        {
            [2]time.Time{time.Unix(1000, 0), time.Unix(1001, 0)},
            false,
        },
    };
    for _,v:=range(args){
        res:=TimeAfterOrEq(v.times[0], v.times[1]);
        if res != v.result {
            t.Errorf("Time %d after or equal to %d: %t\n", v.times[0].Unix(), v.times[1].Unix(), res);
        } else {
            t.Logf("Time %d after or equals %d: %t. OK\n", v.times[0].Unix(), v.times[1].Unix(), res);
        }
    }
}

